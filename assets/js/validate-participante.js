jQuery(function($)
{

		$("#form_participante").validate({
			
			rules: {
				nomecompleto: "required",
				datanascimento: "required",
				cpf: "required",
				areaatuacao:{
					required: true,
				},
				nivel:{
					required: true,
				},
				senha: {
					required: true,
					minlength: 5
				},
				email: {
					required: true,
					email: true
				},
				cep: {
					required: true
				},
				endereco: {
					required: true
				},
				numero: {
					required: true
				},
				bairro: {
					required: true
				},
				estado: {
					required: true
				},
				cidade: {
					required: true
				}
			},
			messages: {
				nomecompleto: "Digite o seu nome completo",
				datanascimento: "Digite a sua data de nascimento",
				cpf: "Digite o seu cpf",
				areaatuacao:{
					required: "Selecione uma área de atuação",
				},
				nivel:{
					required: "Selecione o  nível hierárquico",
				},
				senha: {
					required: "Digite uma senha de acesso",
					minlength: "Mínimo de 6 caracteres"
				},
				email: {
					required: "Digite o seu e-mail",
					email: "Digite um e-mail válido"
				},
				cep: {
					required: "Digite o seu cep",
				},
				endereco: {
					required: "Digite o seu endereço",
				},
				numero: {
					required: "Digite o número"
				},
				bairro: {
					required: "Digite o seu bairro"
				},
				estado: {
					required: "Selecione o Estado"
				},
				cidade: {
					required: "Selecione a Cidade"
				}
			}
		});
});