jQuery(function($)
{

	
		$("#form_cadastro").validate({
			
			rules: {
				tipo_pessoa: "required",
				nomecompleto: "required",
				datanascimento: "required",
				cpf: "required",
				areaatuacao:{
					required: true,
				},
				nivel:{
					required: true,
				},
				senha: {
					required: true,
					minlength: 5
				},
				email: {
					required: true,
					email: true
				},
				cep: {
					required: true
				},
				endereco: {
					required: true
				},
				numero: {
					required: true
				},
				bairro: {
					required: true
				},
				estado: {
					required: true
				},
				cidade: {
					required: true
				},
				razaosocial: {
					required: true
				},
				cnpj: {
					required: true
				},
				cep_empresa: {
					required: true
				},
				endereco_empresa: {
					required: true
				},
				numero_empresa: {
					required: true
				},
				bairro_empresa: {
					required: true
				},
				estado_empresa: {
					required: true
				},
				cidade_empresa: {
					required: true
				}
			},
			messages: {
				tipo_pessoa: "Selecione o tipo de cadastro",
				nomecompleto: "Digite o seu nome completo",
				datanascimento: "Digite a sua data de nascimento",
				cpf: "Digite o seu cpf",
				areaatuacao:{
					required: "Selecione uma área de atuação",
				},
				nivel:{
					required: "Selecione o  nível hierárquico",
				},
				senha: {
					required: "Digite uma senha de acesso",
					minlength: "Mínimo de 6 caracteres"
				},
				email: {
					required: "Digite o seu e-mail",
					email: "Digite um e-mail válido"
				},
				cep: {
					required: "Digite o seu cep",
				},
				endereco: {
					required: "Digite o seu endereço"
				},
				numero: {
					required: "Digite o número"
				},
				bairro: {
					required: "Digite o seu bairro"
				},
				estado: {
					required: "Selecione o Estado"
				},
				cidade: {
					required: "Selecione a Cidade"
				},
				razaosocial: {
					required: "Digite a Razão Social"
				},
				cnpj: {
					required: "Digite o CNPJ"
				},
				cep_empresa: {
					required: "Digite o seu cep"
				},
				endereco_empresa: {
					required: "Digite o seu endereço"
				},
				numero_empresa: {
					required: "Digite o número"
				},
				bairro_empresa: {
					required: "Digite o seu bairro"
				},
				estado_empresa: {
					required: "Selecione o Estado"
				},
				cidade_empresa: {
					required: "Selecione a Cidade"
				}
			},

			eachInvalidField : function() {
        		$(this).css('background', '#F00');
    		},

		    // função chamada para cada campo válido
		    eachValidField : function() { 
		        $(this).css('background', '#0F0');
		    }
		});
});