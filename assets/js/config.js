jQuery(function($)
{
   
   $("#datanascimento").mask("99/99/9999");
   $("#cpf").mask("999.999.999-99");
   $("#cep").mask("99999-999");
   $("#cep_empresa").mask("99999-999");
   $('#cnpj').mask('99.999.999/9999-99');
   $(".formatacao_data_evento").mask("99/99/9999 99:99:00");
   $(".valor").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

   //Quando o campo cep perde o foco.
    $("#cep").blur(function() 
    {

    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") 
    {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) 
        {

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) 
            {

                if (!("erro" in dados)) 
                {
                    //Atualiza os campos com os valores da consulta.
                    $("#endereco").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#cidade").val(dados.localidade);
                    $("#uf").val(dados.uf);
                } 
            });
        } 
        else 
        {
            //cep é inválido.
            limpa_formulario_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else 
    {
        //cep sem valor, limpa formulário.
        limpa_formulario_cep();
    }
	});


     //Quando o campo cep perde o foco.
    $("#cep_empresa").blur(function() 
    {
 
    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');

    //Verifica se campo cep possui valor informado.
    if (cep != "") 
    {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) 
        {

            //Consulta o webservice viacep.com.br/
            $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) 
            {

                if (!("erro" in dados)) 
                {
                    //Atualiza os campos com os valores da consulta.
                    $("#endereco_empresa").val(dados.logradouro);
                    $("#bairro_empresa").val(dados.bairro);

                } 
            });
        } 
        else 
        {
            //cep é inválido.
            limpa_formulario_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else 
    {
        //cep sem valor, limpa formulário.
        limpa_formulario_cep();
    }
    });


$("select[name='estado']").change(function()
{

	var estado =  $(this).val();
    buscar_cidades(estado);
	
}); 

$("select[name='estado_empresa']").change(function()
{

    var estado =  $(this).val();
    buscar_cidades(estado, null, 'pessoa-juridica');
    
}); 

    function buscar_cidades(estado, cidade_selecionada = null, tipo = 'pessoa-fisica')
    {
  
            $.ajax
            ({
                url: 'http://meuevento.de/projeto/estados/pesquisar',
                type: 'GET', 
                data: {estado: estado},
                dataType: 'json',
                success: function(data) 
                {   
                    var opcoes = '';

                    $.each(data.cidade, function (i, item)
                    {
                        if(item.id == cidade_selecionada)
                        {
                            opcoes += "<option value='" + item.id + "' selected>" + item.nome + "</option>";
                        }
                        else
                            opcoes += "<option value='" + item.id + "'>" + item.nome + "</option>";
                    });

                    if(tipo == 'pessoa-fisica')
                        $("select[name='cidade']").html(opcoes);

                    if(tipo == 'pessoa-juridica')
                        $("select[name='cidade_empresa']").html(opcoes);

                }
            }); 
    }

    function limpa_formulario_cep() 
    {
        // Limpa valores do formulário de cep.
        $("#rua").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#uf").val("");
        $("#ibge").val("");
    }

    if($('input:hidden[name=callback_cidade]').length > 0)
    {
        var estado = $("select[name='estado']").val();
        var cidade_selecionada = $('input:hidden[name=callback_cidade]').val();
        buscar_cidades(estado, cidade_selecionada);
    }


    if($('input:hidden[name=callback_cidade_empresa]').length > 0)
    {
        var estado = $("select[name='estado_empresa']").val();
        var cidade_selecionada = $('input:hidden[name=callback_cidade_empresa]').val();
        buscar_cidades(estado, cidade_selecionada, tipo = 'pessoa-juridica');
    }

    $('.tipo_pessoa').click(function(){
        
        var tipo_pessoa = $(this).val();
        
        if(tipo_pessoa == 'pf')
        {
            $('.pj').hide('slow');
        }
        else
        {
            $('.pj').show('slow');
        }
        
    });

    $("select[name='tipo']").change(function()
    {
        var tipo =  $(this).val();
        ocultar_campo_valor(tipo);
        
    }); 

    $('input:hidden[name=callback_tipo]').change(function()
    {
        var tipo =  $(this).val();
        ocultar_campo_valor(tipo);
        
    }); 

    function ocultar_campo_valor(tipo)
    {
 
        if(tipo == 0)
        {
            $('#ocultar_valor').hide('slow');
        }
        else
        {
            $('#ocultar_valor').show('slow');
        }
    }



});