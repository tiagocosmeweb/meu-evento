<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EnderecoEmpresa_model extends CI_model
{
	private $tabela = 'empresa_endereco';

	public function __construct()
	{
		parent::__construct();
	}

	public function salvar($data, $tipo = 'inserir')
	{

		if($tipo == 'inserir')
		{
			$this->db->insert($this->tabela, $data);
			return $this->db->insert_id();
		}
	}

}

?>