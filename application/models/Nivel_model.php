<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Nivel_model extends CI_model
{
	private $tabela = 'nivel';

	public function __construct()
	{
		parent::__construct();
	}

	public function selecionar()
	{
		$this->db->select('id, nivel');
		return $this->db->get($this->tabela)->result_array();
	}

}
?>