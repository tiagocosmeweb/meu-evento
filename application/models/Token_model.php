<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Token_model extends CI_model
{
	private $tabela = 'token';

	public function __construct()
	{
		parent::__construct();
	}

	public function salvar($data, $tipo = 'inserir')
	{
		if($tipo == 'inserir')
		{
			$this->db->insert($this->tabela, $data);
			return $this->db->insert_id();
		}
	}

	public function ativador_participante($email, $chave_ativacao)
	{
		$this->db->select('t.id as idtoken, p.id as idparticipante, situacao');
		$this->db->from('token t');
		$this->db->join('participante p', 'p.id = t.participante_id');
		$this->db->where('p.email', $email);
		$this->db->where('t.token', $chave_ativacao);
		return $this->db->get()->result();
	}

	public function ativador_organizador($email, $chave_ativacao)
	{
		$this->db->select('t.id as idtoken, o.id as idorganizador, situacao');
		$this->db->from('token t');
		$this->db->join('organizador o', 'o.id = t.organizador_id');
		$this->db->where('o.email', $email);
		$this->db->where('t.token', $chave_ativacao);
		return $this->db->get()->result();
	}


	public function apagar($id)
	{
		$this->db->delete($this->tabela, array('id' => $id)); 
		return $this->db->affected_rows();
	}

}
?>