<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Evento_model extends CI_model
{
	private $tabela = 'evento';

	public function __construct()
	{
		parent::__construct();
	}

	public function salvar($data, $tipo = 'inserir', $id = null)
	{

		if($tipo == 'inserir')
		{
			$this->db->insert($this->tabela, $data);
			return $this->db->insert_id();
		}

		else
		{
			$this->db->where('id', $id);
			return $this->db->update($this->tabela, $data); 
		}
	}

	public function condicoes($condicoes, $campos = '*')
	{
		$this->db->select($campos);
		$this->db->where($condicoes);
		return $this->db->get($this->tabela)->result_array();
	}

	public function total_condicoes($condicoes, $campos = '*')
	{
		$this->db->select($campos);
		$this->db->where($condicoes);
		$this->db->from($this->tabela);
		return $this->db->count_all_results();
	}

	public function get($id, $campos = '*')
	{
		$this->db->select($campos);
		
		if(is_int($id))
		{
			return $this->db->get_where($this->tabela, array('id' => $id))->result();
		}

		if(is_array($id))
		{
			return $this->db->get_where($this->tabela, $id)->result();
		}
	}

	public function ultimos($inicio)
	{
		$this->db->where('situacao', 1);
		$this->db->order_by("id", "desc");
		$this->db->limit(3, $inicio);
		return $this->db->get($this->tabela)->result_array();	
	}


	public function apagar($id)
	{
		$this->db->delete($this->tabela, array('id' => $id)); 
		return $this->db->affected_rows();
	}

	public function relatorio_por_sexo($evento_id)
	{

		$sql = $this->db->query("select p.sexo, COUNT(*) as total, i.situacao as inscricao, c.nome, es.nome, es.uf
								from participante p, inscricao i, cidade c, endereco e, estado es
								where  p.id = i.participante_id and evento_id = " . $evento_id . " and p.id = e.participante_id and 
								e.cidade_id = c.id and es.id = e.estado_id
								group by p.sexo, e.estado_id, e.cidade_id");


		/* $sql = $this->db->query("select p.sexo, COUNT(*) as total, i.situacao as inscricao
		from participante p, inscricao i
		where  p.id = i.participante_id and evento_id =  " . $evento_id. "
		group by p.sexo, i.situacao"); */

		return $sql->result_array();
	}

}

?>