<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AreaAtuacao_model extends CI_model
{
	private $tabela = 'area';

	public function __construct()
	{
		parent::__construct();
	}

	public function selecionar()
	{
		$this->db->select('id, area');
		return $this->db->get($this->tabela)->result_array();
	}

}
?>