<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Endereco_model extends CI_model
{
	private $tabela = 'endereco';

	public function __construct()
	{
		parent::__construct();
	}

	public function salvar($data, $tipo = 'inserir')
	{

		if($tipo == 'inserir')
		{
			$this->db->insert($this->tabela, $data);
			return $this->db->insert_id();
		}
	}

	public function get($id, $campos = '*')
	{
		$this->db->select($campos);
		
		if(is_int($id))
		{
			return $this->db->get_where($this->tabela, array('id' => $id))->result();
		}

		if(is_array($id))
		{
			return $this->db->get_where($this->tabela, $id)->result();
		}
	}

	public function condicoes($condicoes, $campos = '*')
	{
		$this->db->select($campos);
		return $this->db->get_where($this->tabela, $condicoes)->result();
	}


}

?>