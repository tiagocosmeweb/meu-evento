<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa_model extends CI_model
{
	private $tabela = 'empresa';

	public function __construct()
	{
		parent::__construct();
	}

	public function salvar($data, $tipo = 'inserir', $id = null)
	{

		if($tipo == 'inserir')
		{
			$this->db->insert($this->tabela, $data);
			return $this->db->insert_id();
		}

		else
		{
			$this->db->where('id', $id);
			return $this->db->update($this->tabela, $data); 
		}
	}

	public function get($id, $campos = '*')
	{
		$this->db->select($campos);
		
		if(is_int($id))
		{
			return $this->db->get_where($this->tabela, array('id' => $id))->result();
		}

		if(is_array($id))
		{
			return $this->db->get_where($this->tabela, $id)->result();
		}
	}


	public function apagar($id)
	{
		$this->db->delete($this->tabela, array('id' => $id)); 
		return $this->db->affected_rows();
	}

}

?>