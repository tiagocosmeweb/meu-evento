<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cidade_model extends CI_model
{
	private $tabela = 'cidade';

	public function __construct()
	{
		parent::__construct();
	}


	public function selecionar_por_estado($id_estado)
	{
		$this->db->select('id, nome');
		$this->db->where('estado_id', $id_estado);
		return $this->db->get($this->tabela)->result_array();
	}

	public function get($id, $campos = '*')
	{
		$this->db->select($campos);
		
		if(is_int($id))
		{
			return $this->db->get_where($this->tabela, array('id' => $id))->result();
		}

		if(is_array($id))
		{
			return $this->db->get_where($this->tabela, $id)->result();
		}
	}

	public function condicoes($condicoes, $campos = '*')
	{
		$this->db->select($campos);
		return $this->db->get_where($this->tabela, $condicoes)->result();
	}

}
?>