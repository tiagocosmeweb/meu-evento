<!DOCTYPE html>
<html lang="pt-br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Meu evento - Publique o seu evento grátis</title>
    <?= link_tag("assets/css/bootstrap.min.css", "stylesheet", "text/css"); ?>
    <?= link_tag("assets/css/font-awesome.min.css", "stylesheet", "text/css"); ?>
    <?= link_tag("assets/css/main.css", "stylesheet", "text/css"); ?>
    <?= link_tag("assets/css/animate.css", "stylesheet", "text/css"); ?>
    <?= link_tag("assets/css/responsive.css", "stylesheet", "text/css"); ?>
    <?= link_tag("assets/css/estilo.css", "stylesheet", "text/css"); ?>

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->  

</head><!--/head-->