 <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> Meu Evento - Publique o seu evento online </p>           
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script type="text/javascript" src="<?= base_url('assets/js/jquery.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.validate.min.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/validate.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.maskedinput.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.maskMoney.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/config.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/bootstrap.min.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/smoothscroll.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/jquery.parallax.js');?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/coundown-timer.js');?>"></script>
  
    <script type="text/javascript" src="<?= base_url('assets/js/main.js');?>"></script>
</body>
</html>