<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div id="event-carousel" class="carousel slide" data-interval="false">
						<h2 class="heading">Últimos eventos</h2>
						<a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
						<a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">

								<?php

								foreach($fila1_eventos as $evento)
								{
								 ?>

								 	<div class="col-sm-3">
										<div class="single-event">
											<h4><?= $evento['titulo']; ?></h4>
											<h5><?= date('d/m/Y h:i:s', strtotime($evento['data_inicio'])); ?></h5>
											<h4><?= anchor(url_title($evento['titulo'], '-', true) .'/'. $evento['id'], "Saiba mais", 'class = "btn btn-primary"'); ?></h4>
										</div>
									</div>

								 <?php
								 }
								 ?>
							
								</div>
							</div>

							<div class="item">
								<div class="row">

								<?php
								
								foreach($fila2_eventos as $evento)
								{
								 ?>

								 	<div class="col-sm-3">
										<div class="single-event">
											<h4><?= $evento['titulo']; ?></h4>
											<h5><?= date('d/m/Y h:i:s', strtotime($evento['data_inicio'])); ?></h5>
											<h4><?= anchor(url_title($evento['titulo'], '-', true) .'/'. $evento['id'], "Saiba mais", 'class = "btn btn-primary"'); ?></h4>
										</div>
									</div>

								 <?php
								 }
								 ?>

								</div>
							</div>


								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section><!--/#event-->