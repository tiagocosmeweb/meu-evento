   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row" style = 'min-height: 350px;'>
                  <h2>Mensagem</h2>
                    
                  <?php

                    switch ($mensagem) 
                    {
                      case 'cadastro-sucesso':
                        echo "Cadastro realizado com sucesso. Enviamos um e-mail para confirmar o seu acesso.";
                        break;

                      case 'cadastro-confirmacao':
                        echo "Obrigado por realizar a sua confirmação.";
                        break;

                      case 'cadastro-ja-confirmado':
                        echo "Seu cadastro encontra-se ativo.";
                        break;

                      case 'email-nao-encontrado':
                        echo "E-mail não encontrado em nossa base de dados.";
                        break;

                      case 'email-alterar-senha':
                        echo "Enviamos um e-mail para alterar a sua senha.";
                        break;

                      case 'email-chave-invalida':
                        echo "Chave inválida. Por favor, tente novamente.";
                        break;

                      case 'senha-alterada-sucesso':
                        echo "Senha alterada com sucesso.";
                        break;

                      case 'login-invalido':
                        echo "Dados inválidos. Tente novamente.";
                        break;

                      case 'ativar-cadastro':
                        echo "Para o seu primeiro acesso é necessário a confirmação de seu cadastro que enviamos para o seu e-mail.";
                        break;

                      case 'novo-login':
                        echo "Realize novamente o login para acessar o seu painel de controle.";
                        break;

                      case 'sair':
                        echo "Sua conta foi deslogada com sucesso.";
                        break;

                      case 'cadastro-nao-confirmado':
                        echo "Seu cadastro não encontra-se confirmado em nossa base de dados. Consulte o seu e-mail.";
                        break;
                      
                      default:
                        // redirect('/');
                        break;
                    }
                    
                  ?>
                   
                </div> 
                </div>
            </div>
    </section><!--/#twitter-feed-->
    