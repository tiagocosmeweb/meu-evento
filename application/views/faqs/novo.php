   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Nova questão</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('faqs/salvar', $atributos); 
                    echo form_hidden('evento_id', $evento_id); 
                  ?>

                      <div class="row">

                        <div class="col-md-3">

                            <div class="form-group">
                              
                              <label for="">Pergunta</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "Questão";
                                $atributos['placeholder'] = "Digite sua questão";
                                echo form_input('pergunta', set_value('pergunta'), $atributos);
                                echo form_error('pergunta', '<div class="error">', '</div>');
                              ?>

                            </div>

                        </div>

                        </div>


                    <div class = "row">

                        <div class="col-md-8">

                          <div class="form-group">
                              <label for="">Resposta</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua resposta";
                                  $atributos['id'] = "resposta";
                                  echo form_textarea('resposta', set_value('resposta'), $atributos);
                                  echo form_error('resposta', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                    </div>


                    </div>
                        <?php 
                        echo form_submit('btn', 'inserir');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    