   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Editar questão</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('faqs/atualizar', $atributos); 
                    echo form_hidden('evento_id', $evento_id); 
                    echo form_hidden('id', $questao[0]->id);

                  ?>

                      <div class="row">

                        <div class="col-md-3">

                            <div class="form-group">
                              
                              <label for="">Pergunta</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "Questão";
                                $atributos['placeholder'] = "Digite sua questão";
                                $valor = (set_value('pergunta')) ? set_value('pergunta') : $questao[0]->pergunta;
                                echo form_input('pergunta', $valor, $atributos);
                                echo form_error('pergunta', '<div class="error">', '</div>');
                              ?>

                            </div>

                        </div>

                        </div>


                    <div class = "row">

                        <div class="col-md-8">

                          <div class="form-group">
                              <label for="">Resposta</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua resposta";
                                  $atributos['id'] = "resposta";
                                  $valor = (set_value('resposta')) ? set_value('resposta') : $questao[0]->resposta;
                                  echo form_textarea('resposta', $valor, $atributos);
                                  echo form_error('resposta', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                    </div>


                      <div class = "row">

                        <div class="col-md-8">

                          <div class="form-group">
                              <label for="">Situação</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua resposta";
                                  $atributos['id'] = "resposta";
                                  $situacoes = array('1' => 'Publicado', '0' => 'Não publicado');
                                  $valor = (set_value('situacao')) ? set_value('situacao') : $questao[0]->situacao;
                                  echo form_dropdown('situacao', $situacoes, $valor, $atributos);
                                  echo form_error('situacao', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                    </div>




                    </div>
                        <?php 
                        echo form_submit('btn', 'alterar');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    