   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'min-height: 350px;'>          
                
                <h1> Faq </h1>

                <p> <?= $evento_selecionado[0]->titulo; ?></p>

                <?php

                echo anchor('faqs/novo/' . $evento_id, "Nova questão", "class = 'btn btn-primary'");

                ?>

                <?php

                echo br(2);

                $mensagem = $this->session->flashdata('mensagem');

                if($mensagem != null)
                {
                ?>

                      <span class = 'alert alert-success'>
                        <?= $this->session->flashdata('mensagem');  ?>
                    </span>

                <?php
                }
                
                if(count($questoes) >= 1)
                {

                ?>

                 <table class = "table table-bordered table-hover" style= 'margin-top: 20px;'>
                 <tr>
                    <th> Nome </th>
                    <th> Deletar </th>
                    <th> Editar </th>
                    <th> Situação </th>
                 </tr>


                <?php
                 	foreach($questoes as $questao)
                 	{

                ?>

                	<tr class = '<?= ($questao->situacao == 0) ? "danger" : "success"; ?>'>
                 		<td width='80%'> <?= $questao->pergunta; ?></td>
                 	  
                    <td>   
                        <?= anchor('faqs/deletar/' . $questao->id . "?evento=" .$evento_id, "<i class = 'glyphicon glyphicon-remove'></i>"); ?> 
                    </td>

                    <td>   
                        <?= anchor('faqs/editar/' . $questao->id . "/" .$evento_id, "<i class = 'glyphicon glyphicon-pencil'></i>"); ?> 
                    </td>

                    <td>
                      <?= ($questao->situacao == 1) ? 'Publicado' : 'Não publicado'; ?>
                    </td>
                      
                 	</tr>
                <?php
            		}
                ?>
                </table>

                <?php
             	}

              else
              {
                  echo "<p style = 'margin-top: 20px;'> Não há faq disponível no momento. </p>";
              }

              ?>
                
                </div>
        </div>     

    </section>
  