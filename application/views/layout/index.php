<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	
	<title><?= $evento[0]->titulo; ?></title>

	<link rel="shortcut icon" href="assets/images/gt_favicon.png">
	
	<!-- Bootstrap itself -->
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">

	<!-- Custom styles -->
	<?= link_tag("assets_participante/css/magister.css", "stylesheet", "text/css"); ?>
	<!-- Fonts -->
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Wire+One' rel='stylesheet' type='text/css'>
</head>

<!-- use "theme-invert" class on bright backgrounds, also try "text-shadows" class -->
<body class="theme-invert">

<nav class="mainmenu">
	<div class="container">
		<div class="dropdown">
			<button type="button" class="navbar-toggle" data-toggle="dropdown"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
			<!-- <a data-toggle="dropdown" href="#">Dropdown trigger</a> -->
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li><a href="#sobre" class="active">Sobre</a></li>
				<li><a href="#descricao">Descrição</a></li>
				<li><a href="#palestrantes">Palestrantes</a></li>
				<li><a href="#videos">Vídeos & Conteúdos</a></li>
				<li><a href="#faq">FAQ</a></li>
			</ul>
		</div>
	</div>
</nav>

<!-- Main (Home) section -->
<section class="section" id="sobre">
	<div class="container">

		<div class="row">
			<div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 text-center">	

				<!-- Site Title, your name, HELLO msg, etc. -->
				<h1 class="title"><?= $evento[0]->titulo; ?></h1>
				<h2 class="subtitle">
			
					<?= $endereco[0]->local ?> - <?= $endereco[0]->endereco ?>, <?= $endereco[0]->numero ?>  <?= $estado[0]->nome ?>, <?= $estado[0]->uf ?> |
					<?= date('d/m/Y', strtotime($evento[0]->data_inicio)) ?>, <?= date('h:i:s', strtotime($evento[0]->data_inicio)) ?> horas  - <?= date('d/m/Y', strtotime($evento[0]->data_final)) ?>, <?= date('h:i:s', strtotime($evento[0]->data_final)) ?> horas.
				</h2>


				<h2 class="subtitle">
			
					<p style = 'padding-top: 15px;'> <b> <?= ($evento[0]->tipo == 1) ? 'Investimento R$' . number_format($evento[0]->valor, 2, ',', '.')  : 'Evento gratuito' ; ?>  </b> </p>

					<?php

						$total_de_vagas = (int) $evento[0]->numero_vaga - $total_inscritos;

						if($total_de_vagas > 0)
						{

					?>

						<p style = 'padding-top: 15px;'> Resta(m) <?= $total_de_vagas; ?> vaga(s). </p>
						<!-- Nice place to describe your site in a sentence or two -->
						<p  style = 'padding-top: 15px;'> <?= anchor('/usuarios/login?cadastro=participante' , 'Inscreva-se', 'class="btn btn-default btn-lg"') ?> </p>

					<?php
						}
						else
						{
					?>
							<p> Infelizmente não há mais vagas. </p>
					<?php
						}
					?>
				</h2>

			    <!-- Short introductory (optional) -->
				<h3 class="tagline">
					Conheça mais o evento através do menu.
				</h3>
	
			</div> <!-- /col -->
		</div> <!-- /row -->
	
	</div>
</section>

<!-- Second (About) section -->
<section class="section" id="descricao">
	<div class="container">
	
		<h2 class="text-center title">Conheça mais</h2>
		<div class="row">
			<div class="col-sm-10">    
				<p><?= $evento[0]->descricao; ?></p>    
			</div>
		</div>
	</div>
</section>

<!-- Third (Works) section -->
<section class="section" id="palestrantes">
	<div class="container">
	
		<h2 class="text-center title">Palestrantes</h2>

		<?php
			if(count($palestrantes) == 0)
			{
			?>

				<p class="lead text-center">
					Não há palestrantes cadastrados.
				</p>

			<?php
			}
			else
			{
				foreach($palestrantes as $palestrante)
				{
		?>

		<div class="row">
			<div class="col-sm-4 col-sm-offset-2">
				<div class="thumbnail">
					<img src="assets/screenshots/sshot1.jpg" alt="">
					<div class="caption">
					    <?= img('uploads/' . $palestrante->evento_id . '/' . $palestrante->foto, null, 'class="img-circle text-center"'); ?>
						<h3><?= $palestrante->nome; ?></h3>
						<p><?=  $palestrante->resumo_profissional; ?></p>
					</div>
				</div>
			</div>
		<?php
				}
			}
		?>
			
		</div>

	</div>
</section>


<section class="section" id="videos">
	<div class="container">
	
		<h2 class="text-center title">Vídeos</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">

				<?php
					if(count($videos) == 0)
					{
					?>

						<p class="lead text-center">
							Não há vídeos cadastrados.
						</p>

					<?php
					}
					else
					{
						foreach($videos as $video)
						{
				?>

				<p class="lead"> 
					<p> <b> <?= $video->titulo; ?> </b></p>    
					<p><?= $video->descricao; ?></p>   
					<p><?= $video->video_incorporado; ?></p>    
				</p>
				<?php
					}
				}
				?>
				
			</div>
		</div>

		<h2 class="text-center title">Conteúdos</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-center">

				<?php
					if(count($conteudos) == 0)
					{
					?>

						<p class="lead text-center">
							Não há conteúdos cadastrados.
						</p>

					<?php
					}
					else
					{
						foreach($conteudos as $conteudo)
						{
				?>

				<p class="lead"> 
					<?= anchor('uploads/' . $conteudo->evento_id .'/'. $conteudo->arquivo, $conteudo->titulo); ?> 
				</p>
				<?php
					}
				}
				?>
				
			</div>
		</div>

	</div>
</section>


<section class="section" id="faq">
	<div class="container">
	
		<h2 class="text-center title">FAQ</h2>

		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 text-left">

				<?php
					if(count($faq) == 0)
					{
					?>

						<p class="lead text-center">
							Não há FAQ disponível.
						</p>

					<?php
					}
					else
					{
						foreach($faq as $aux)
						{
					     // echo "<pre>" . print_r($aux, true) . "</pre>";
				?>

				<p class="lead" > 
					<h4> <i class = 'glyphicon glyphicon-circle-arrow-right'></i>
						<b> <?= $aux->pergunta; ?> </b>
					</h4>    
					<p><?= $aux->resposta; ?></p>   
				</p>
				<?php
					}
				}
				?>
				
			</div>
		</div>

	</div>
</section>


<!-- Load js libs only when the page is loaded. -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="assets/js/modernizr.custom.72241.js"></script>
<!-- Custom template scripts -->
<script src="assets/js/magister.js"></script>
<script type="text/javascript" src="<?= base_url('assets_participante/js/magister.js');?>"></script>
</body>

	<h3 class="tagline" style= 'text-align: center;'>
		<a href= "http://www.meuevento.de/projeto/home">Buscar outros eventos</a>
	</h3>

</html>