   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'min-height: 350px;'>
                
                  <h1 "style = 'margin-bottom: 20px;'"> Painel de Controle </h1>

                  <?php

                    $mensagem = $this->session->flashdata('mensagem');

                    if($mensagem != null)
                    {
                    ?>

                          <span class = 'alert alert-success'>
                            <?= $this->session->flashdata('mensagem');  ?>
                        </span>

                    <?php
                    }
                    ?>  

                    <?php    

                    if(is_array($evento_selecionado))
                    {
                      echo " <p class = 'alert alert-success'> Deseja confirmar sua inscrição para o evento " .  $evento_selecionado[0]->titulo . "?</p>";

                      echo anchor('participantes/inscricao/', 'confirmar inscrição', 'class= "btn btn-primary"');
                    }
                  
                  	if(count($inscricoes) == 0)
                  	{
                  	?>

                  	<p class= 'alert'> <b> <?= $this->sessao->get('nome') . ' </b>, você não está participando de nenhum evento até o momento!'; ?></p>

                  	<?php

                  	}

                  	else
                  	{

                    ?>

                    <table class = "table table-bordered table-hover" style = "margin-top: 40px;">
                    <tr>
                      <th> Evento </th>
                      <th> Situação cadastral </th>
                      <th> Gerenciar </th>
                    </tr>

                    <?php

                      foreach($inscricoes as $inscricao)
                      {
                        // echo "<pre>" . print_r($inscricao, true) . "</pre>";
                    
                    ?>

                      <tr class = "<?= ($inscricao['inscricao']->situacao == 0) ? 'danger' : 'success'; ?>">
                          <th> <?= $inscricao['evento'][0]->titulo; ?> </th>
                          <th> 
                              <?= ($inscricao['inscricao']->situacao == 0) ? 'Cancelado' : 'Cadastrado'; ?> 
                          </th>
                          <th>
                              <?php

                                if($inscricao['inscricao']->situacao == 1)
                                {
                                     echo anchor('participantes/cancelar/' . $inscricao['inscricao']->evento_id , 'cancelamento', 'class= "btn btn-primary"');
                                }
                                else
                                {
                                  echo "------";
                                }

                              ?>


                          </th>
                      </tr>

                      <?php
                      }

                  	}
                  ?>

                  </table>
        
                </div>
        </div>     

    </section>
  