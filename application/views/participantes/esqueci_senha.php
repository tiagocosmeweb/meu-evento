   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Novo Participante</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_participante'; 
                    echo form_open('esqueci-senha', $atributos); 
                  ?>

                    <div class="row">
                      <div class="col-md-4">
 
                        <div class="form-group">
                           <label for="">E-mail</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu E-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>       
                        </div>
 
                        <div class="form-group">
                           <label for="">Meu cadastro é: </label>
                            <?php
                               $atributos = null;
                               $atributos['class']       = "form-control";
                               $tipocadastro = array('' => 'Selecione', 'participante' => 'Participante', 'organizador' => 'Organizador');
                               echo form_dropdown('tipocadastro', $tipocadastro, set_value('tipocadastro'), $atributos);
                            ?>
                        </div>

                        <div>
                          <?php 
                          echo form_submit('btn', 'cadastrar');
                          echo form_close(); ?>
                      </div>  

                      </div>
               
                  </div> 
                </div>
              </div>
            </div> 
    </section><!--/#twitter-feed-->
    