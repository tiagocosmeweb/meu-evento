   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open('participantes/salvar', $atributos); 
                  ?>

                    <div class="row">
                      <div class="col-md-4">
 
                        <div class="form-group">
                            <label for="">Nome completo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Digite o seu nome";
                                echo form_input('nomecompleto', set_value('nomecompleto'), $atributos);
                                echo form_error('nomecompleto', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>

                        <div class="col-md-2">

                          <div class="form-group">
                              <label for="">Data de nascimento</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua data de nascimento";
                                  $atributos['id'] = "datanascimento";
                                  echo form_input('datanascimento', set_value('datanascimento'), $atributos);
                                  echo form_error('datanascimento', '<div class="error">', '</div>');
                                ?>
                          </div>

                        </div>

                        <div class="col-md-2">

                          <div class="form-group">
                           <label for="">CPF</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu CPF";
                                  $atributos['id'] = "cpf";
                                  echo form_input('cpf', set_value('cpf'), $atributos);
                                  echo form_error('cpf', '<div class="error">', '</div>');
                                ?>
                          </div>
                      </div>
                    </div>

                      <div class="row">

                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="">Área de atuação</label>
                            
                            <?php
                              $atributos = null;
                              $atributos['class'] = "form-control";
                              echo form_dropdown('areaatuacao', $areaatuacao, set_value('areaatuacao'), $atributos);
                              echo form_error('areaatuacao', '<div class="error">', '</div>');
                            ?>

                          </div>

                        </div>

                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="">Nível</label>
                            
                            <?php
                              $atributos = null;
                              $atributos['class'] = "form-control";
                              echo form_dropdown('nivel',$nivel, set_value('nivel') , $atributos);
                              echo form_error('nivel', '<div class="error">', '</div>');
                            ?>
                          </div>
                        </div>
                    </div>

                      <h2> Dados de acesso </h2>

                      <div class="row">

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">E-mail</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu E-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>                        
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Senha</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua senha";
                                  echo form_password('senha', set_value('senha'), $atributos);
                                  echo form_error('senha', '<div class="error">', '</div>');
                                ?>      
                            </div>

                        </div>

                      </div>     

                    <h2> Endereço </h2>

                      <div class="row">

                        <div class="col-md-3">

                            <div class="form-group">
                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu cep";
                                  $atributos['id'] = "cep";
                                  echo form_input('cep', set_value('cep'), $atributos);
                                  echo form_error('cep', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>
                      </div>

                      <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
                              <label for="">Endereço</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu endereço";
                                  $atributos['id'] = "endereco";
                                  echo form_input('endereco', set_value('endereco'), $atributos);
                                  echo form_error('endereco', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                              <label for="">Número</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o número";
                                  $atributos['id'] = "numero";
                                  echo form_input('numero', set_value('numero'), $atributos);
                                  echo form_error('numero', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="exampleInputPassword1">Complemento</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o complemento";
                                  $atributos['id'] = "numero";
                                  echo form_input('complemento', set_value('complemento'), $atributos);
                                  echo form_error('complemento', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Bairro</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o bairro";
                                  $atributos['id'] = "bairro";
                                  echo form_input('bairro', set_value('bairro'), $atributos);
                                  echo form_error('bairro', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                    </div>


                    <div class="row">


                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Estado</label>
 
                              <?php
                                $atributos = null;
                                $atributos['class'] = "form-control selecionar_cidade";
                                $atributos['id'] = "estado";
                                echo form_dropdown('estado', $estados, set_value('estado'), $atributos);
                                echo form_error('estado', '<div class="error">', '</div>');
                              ?>                            
                            </div>

                        </div>

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Cidade</label>
                              <?php
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $atributos['id'] = "cidade";
                                  echo form_dropdown('cidade', array('' => 'Selecione o Estado'), set_value('cidade'), $atributos);
                                  echo form_error('cidade', '<div class="error">', '</div>');
                                  echo form_hidden('callback_cidade', set_value('cidade'));
                              ?>    
                            </div>

                        </div>

                    </div>
                        <?php 
                        echo form_submit('btn', 'cadastrar');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    