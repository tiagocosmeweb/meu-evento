   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Modificar senha</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_participante'; 
                    echo form_open('usuarios/salvar_senha', $atributos); 
                  ?>

                    <div class="row">
                      <div class="col-md-4">
 
                        <div class="form-group">
                              <label for="">Nova senha</label>

                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua senha";
                                  echo form_password('senha', set_value('senha'), $atributos);
                                  echo form_error('senha', '<div class="error">', '</div>');

                                  echo form_hidden('email', isset($email) ? $email : set_value('email'));
                                  echo form_hidden('chave_ativacao',  isset($chave_ativacao) ? $chave_ativacao : set_value('chave_ativacao'));
                                   echo form_hidden('tipo_cadastro',  isset($tipo_cadastro) ? $tipo_cadastro : set_value('tipo_cadastro'));
                                ?>   

                            </div>

                        <div>

                          <?php 
                          echo form_submit('btn', 'modificar senha');
                          echo form_close(); ?>
                      </div>  

                      </div>
               
                  </div> 
                </div>
              </div>
            </div>  
    </section><!--/#twitter-feed-->
    