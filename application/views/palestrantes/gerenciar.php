   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'padding-bottom: 25px;'>
          
                <h1 style = 'padding-bottom: 15px;'> Palestrantes </h1>

                <?php

                $mensagem = $this->session->flashdata('mensagem');

                if($mensagem != null)
                {
                ?>

                      <span class = 'alert alert-success'>
                        <?= $this->session->flashdata('mensagem');  ?>
                    </span>

                <?php
                }
                ?>


                <p style = 'padding-top: 25px;'> <b> Evento: </b> <?= $evento_selecionado[0]->titulo; ?></p>

                <?php

                echo anchor('palestrantes/novo/' . $evento_id, "Novo palestrante", "class = 'btn btn-primary'");

                if(count($palestrantes) >= 1)
                {

                ?>

                 <table class = "table table-bordered table-hover" style= 'margin-top: 20px;'>
                 <tr>
                    <th> Nome </th>
                    <th> Deletar </th>
                 </tr>


                <?php
                 	foreach($palestrantes as $palestrante)
                 	{

                ?>

                	<tr>
                 		<td width='80%'> <?= $palestrante->nome; ?></td>
                 	  
                        <td>   
                            <?= anchor('palestrantes/deletar/' . $palestrante->id . "?evento=" .$evento_id, "<i class = 'glyphicon glyphicon-remove'></i>"); ?> 
                        </td>
                      
                 	</tr>
                <?php
            		}
                ?>
                </table>

                <?php
             	}

                else
                {
                    echo "<p style = 'margin-top: 20px;'> Não há palestrantes cadastrados no momento. </p>";
                }

                ?>
                
                </div>
        </div>     

    </section>
  