   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                
                <div class= "row">

                
                  <?php

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open_multipart('palestrantes/salvar', $atributos); 
                    echo form_hidden('evento_id', $evento_id);

                  ?>
                
                <div class= "row">
                    
                  <h3> Novo palestrante </h3>
                  <p> <?= $evento_selecionado[0]->titulo; ?></p>
                  
                    <div class="row">
                      <div class="col-md-12">
 
                        <div class="form-group">
                            <label for="">Nome completo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Digite o nome";
                                echo form_input('nome', set_value('nome'), $atributos);
                                echo form_error('nome', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>

                        <div class="col-md-12">

                          <div class="form-group">
                              <label for="">E-mail</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o e-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>
                          </div>

                        </div>

                        <div class="col-md-12">

                          <div class="form-group">
                           <label for="">Resumo profissional</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $atributos['style'] = "width: 1168px; height: 151px;";

                                  $atributos['placeholder'] = "Resumo profissional do palestrante";
                                  $atributos['id'] = "resumo_profissional";
                                  echo form_textarea('resumo_profissional', set_value('resumo_profissional'), $atributos);
                                  echo form_error('resumo_profissional', '<div class="error">', '</div>');
                                ?>
                          </div>
                      </div>


                        <div class="col-md-12">

                          <div class="form-group">
                           <label for="">Foto do participante</label>
                                <?php 
                                  $atributos = null;
                                  echo form_upload('arquivo');
                                  echo form_error('arquivo', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                      
                    </div>

                </div>

              <?php 
                    echo form_submit('btn', 'cadastrar');
                    echo form_close(); 
              ?>
            </div>
        </div>     

    </section>
  