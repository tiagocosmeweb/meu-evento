   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                
                <div class= "row">

                
                  <?php

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open_multipart('conteudos/salvar', $atributos); 
                    echo form_hidden('evento_id', $evento_id);

                  ?>
                
                <div class= "row">
                    
                  <h3> Novo conteúdo </h3>
                   <p> <?= $evento_selecionado[0]->titulo; ?></p>

                    <div class="row">
                      <div class="col-md-7">
 
                        <div class="form-group">
                            <label for="">Título do conteúdo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Digite o nome";
                                echo form_input('titulo', set_value('titulo'), $atributos);
                                echo form_error('titulo', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>

                        <div class="col-md-12">

                          <div class="form-group">
                           <label for="">Selecione o arquivo</label>
                                <?php 
                                  $atributos = null;
                                  echo form_upload('arquivo');
                                  echo form_error('arquivo', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                      
                    </div>

                </div>

              <?php 
                    echo form_submit('btn', 'cadastrar');
                    echo form_close(); 
              ?>
            </div>
        </div>     

    </section>
  