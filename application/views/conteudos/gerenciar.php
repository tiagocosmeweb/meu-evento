   <section id="search">
            <div class="container">
               <div class="col-sm-12">

               <h1 style = 'padding-bottom: 15px;'> Conteúdos </h1>

                <?php

                $mensagem = $this->session->flashdata('mensagem');

                if($mensagem != null)
                {
                ?>

                      <span class = 'alert alert-success'>
                        <?= $this->session->flashdata('mensagem');  ?>
                    </span>

                <?php
                }
                ?>

                <p style = 'padding-top: 25px;'> <b> Evento: </b> <?= $evento_selecionado[0]->titulo; ?></p>

                <?php

                echo anchor('conteudos/novo/' . $evento_id, "Novo conteúdo", "class = 'btn btn-primary'");

                if(count($conteudos) >= 1)
                {

                ?>

                 <table class = "table table-bordered table-hover" style= 'margin-top: 20px;'>
                 <tr>
                    <th> Título </th>
                    <th> Deletar </th>
                 </tr>


                <?php
                 	foreach($conteudos as $conteudo)
                 	{

                ?>

                	<tr>
                 		<td width='80%'> <?= $conteudo->titulo; ?></td>
                 	  
                        <td>   
                            <?= anchor('conteudos/deletar/' . $conteudo->id . "?evento=" .$evento_id, "<i class = 'glyphicon glyphicon-remove'></i>"); ?> 
                        </td>
                      
                 	</tr>
                <?php
            		}
                ?>
                </table>

                <?php
             	}

                else
                {
                    echo "<p style = 'margin-top: 20px;'> Não há conteúdos cadastrados no momento. </p>";
                }

                ?>
                
                </div>
        </div>     

    </section>
  