   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                
                <div class= "row">

                
                  <?php

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open_multipart('videos/salvar', $atributos); 
                    echo form_hidden('evento_id', $evento_id);

                  ?>
                
                <div class= "row">
                    
                  <h3> Novo vídeo </h3>
                  <p> <?= $evento_selecionado[0]->titulo; ?></p>


                    <div class="row">
                      <div class="col-md-7">
 
                        <div class="form-group">
                            <label for="">Título do conteúdo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Título";
                                echo form_input('titulo', set_value('titulo'), $atributos);
                                echo form_error('titulo', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>

                       <div class="col-md-7">
 
                        <div class="form-group">
                            <label for="">Descrição do conteúdo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Descrição";
                                echo form_input('descricao', set_value('descricao'), $atributos);
                                echo form_error('descricao', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>


                      <div class="col-md-7">
 
                        <div class="form-group">
                            <label for="">Embeed</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Link de vídeo";
                                echo form_input('video_incorporado', set_value('video_incorporado'), $atributos);
                                echo form_error('video_incorporado', '<div class="error">', '</div>');
                              ?>

                        </div>


                        <?php 
                              echo form_submit('btn', 'cadastrar');
                              echo form_close(); 
                        ?>
              
                      </div>
                    </div>

                </div>
            </div>
        </div>     

    </section>
  