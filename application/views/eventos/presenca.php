   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'min-height: 350px;'>
                <div class= "row">
                    
                  <h2>Controle de presença</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('eventos/salvar_presenca', $atributos);
                    echo form_hidden('evento_id', $evento_id);
                    echo form_hidden('participante_id', $participante_id);
                  ?>

                    <div class = "row">

                      <div class="col-md-12">

                          <div class="form-group">
                              <label for="">Confirmar presença?</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $opcoes = array('0' => 'Não confirmado', '1' => 'Presente');
                                  echo form_dropdown('presenca', $opcoes, set_value('opcoes'), $atributos);
                                  echo form_error('presenca', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>
                    </div>
    
                    </div>
                        <?php 
                        echo form_submit('btn', 'salvar');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    