   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'min-height: 350px;'>
                
                <h1 style = 'padding-bottom: 25px;'> Meus eventos </h1>

                <?php

                $mensagem = $this->session->flashdata('mensagem');

                if($mensagem != null)
                {
                ?>

                        <span class = 'alert alert-success'>
                            <?= $this->session->flashdata('mensagem');  ?>
                        </span>

                <?php
                }
                ?>

                 <table class = "table table-bordered table-hover" style = 'margin-top: 45px;'>
                 <tr>
                 	<th> Eventos </th>
                 	<th> Palestrantes </th>
                 	<th> Vídeos </th>
                 	<th> Conteúdos </th>
                 	<th> FAQ </th>
                 	<th> Gerenciar </th>
                    <th> Página </th>
                    <th> E-mail </th>
                    <th> Participantes </th>
                    <th> Relatórios </th>
                    <th> Certificado </th>
                 </tr>

                <?php

                  $atts = array(
				              'width'      => '900',
				              'height'     => '400',
				              'scrollbars' => 'yes',
				              'status'     => 'no',
				              'resizable'  => 'yes',
				              'screenx'    => '0',
				              'screeny'    => '0'
				            );


                if(count($eventos) >= 1)
                {
                 	foreach($eventos as $evento)
                 	{

                 	// echo "<pre>" . print_r($evento,true). "</pre>";
                ?>

                	<tr class = '<?= ($evento['situacao'] == 0) ? "danger" : "success"; ?>'>
                 		
                        <td width='80%'> <?= $evento['titulo']; ?> </td>
                 	    <td>   
                 	    	<?= anchor('palestrantes/gerenciar/' . $evento['id'], "<i class = 'glyphicon glyphicon-user'></i>"); ?> 
                 	    </td>

                         <td>   
                            <?= anchor('videos/gerenciar/' . $evento['id'], "<i class = 'glyphicon glyphicon-paperclip'></i>"); ?> 
                        </td>
                 	    
                        <td> 
                            <?= anchor('conteudos/gerenciar/' . $evento['id'], "<i class = 'glyphicon glyphicon-paperclip'></i>"); ?>  
                        </td>

                        <td> 
                            <?= anchor('faqs/gerenciar/' . $evento['id'], "<i class = 'glyphicon glyphicon-list-alt'></i>"); ?>  
                        </td>

                        <td> 
                            <?= anchor('eventos/editar/' . $evento['id'], "<i class = 'glyphicon glyphicon-pencil'></i>"); ?>  
                        </td>
                        
                        <td>  <?= anchor(url_title($evento['titulo'], '-', true) .'/'. $evento['id'], "<i class = 'glyphicon glyphicon-home'></i>", 'target = "_blank"'); ?> </td>

                        <td> 
                            <?= anchor('eventos/email/' . $evento['id'], "<i class = 'glyphicon glyphicon-envelope'></i>"); ?>  
                        </td>

                        <td> 
                            <?= anchor('eventos/participantes/' . $evento['id'], "<i class = 'glyphicon glyphicon-list-alt'></i>"); ?>  
                        </td>

                        <td> 
                            <?= anchor_popup('eventos/relatorios/' . $evento['id'] . '?relatorio=sexo', "<i class = 'glyphicon glyphicon-stats'></i>", $atts); ?>  
                        </td>

                        <td>
                             <?= anchor('certificados/gerar/' . $evento['id'], "<i class = 'glyphicon glyphicon-ok'></i>"); ?>  
                        </td>

                 	</tr>
                <?php
            		}
             	}
                ?>
                
                </table>
                
                </div>
        </div>     

    </section>
  