   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Novo evento</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('eventos/salvar', $atributos); 
                  ?>

                    <div class="row">
                      <div class="col-md-8">
 
                        <div class="form-group">
                             <label for="">Título do evento</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Título do evento";
                                echo form_input('titulo', set_value('titulo'), $atributos);
                                echo form_error('titulo', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>
                    </div>

                    <div class = "row">

                        <div class="col-md-8">

                          <div class="form-group">
                              <label for="">Descrição do evento</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Descreva o seu evento";
                                  $atributos['id'] = "descricao";
                                  echo form_textarea('descricao', set_value('descricao'), $atributos);
                                  echo form_error('descricao', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>
                    </div>

                      <div class="row">

                        <div class="col-md-3">

                            <div class="form-group">
                              
                              <label for="">Data inicial</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control formatacao_data_evento";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Data e hora inicial";
                                echo form_input('data_inicio', set_value('data_inicio'), $atributos);
                                echo form_error('data_inicio', '<div class="error">', '</div>');
                              ?>

                        </div>

                        </div>

                          <div class="col-md-3">

                            <div class="form-group">
                             <label for="">Data final</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control formatacao_data_evento";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Data e hora final";
                                echo form_input('data_final', set_value('data_final'), $atributos);
                                echo form_error('data_final', '<div class="error">', '</div>');
                              ?>

                        </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                             <label for="">Número de vagas</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Número de vagas";
                                echo form_input('numero_vaga', set_value('numero_vaga'), $atributos);
                                echo form_error('numero_vaga', '<div class="error">', '</div>');
                              ?>
                        </div>

                        </div>
                        </div>

                        <div class= "row">

                        <div class="col-md-4">

                            <div class="form-group">
                             <label for="">Evento gratuito ou pago?</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "tipo";
                                $atributos['placeholder'] = "Número de vagas";
                                
                                $tipo[''] = 'Selecione';
                                $tipo['0'] = 'Grátis';
                                $tipo['1'] = 'Pago';

                                echo form_dropdown('tipo',$tipo, set_value('tipo'), $atributos);
                                echo form_error('tipo', '<div class="error">', '</div>');
                              ?>
                        </div>

                        </div>

                        <div class="col-md-4" id='ocultar_valor'>

                            <div class="form-group">
                             <label for="">Valor</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control valor";
                                $atributos['id']       = "valor";
                                $atributos['placeholder'] = "Valor";

                                echo form_input('valor', set_value('valor'), $atributos);
                                echo form_error('valor', '<div class="error">', '</div>');
                              ?>
                        </div>

                        </div>


                    </div>

                    <h2> Localidade do evento </h2>

                      <div class="row">

                        <div class="col-md-8">
                              <label for="">Local do evento</label>

                            <div class="form-group">
                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Local (nome, hotel, instituição... )";
                                  $atributos['id'] = "local";
                                  echo form_input('local', set_value('local'), $atributos);
                                  echo form_error('local', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>
                      </div>

                      <div class="row">

                       <div class="col-md-3">

                            <div class="form-group">
                              <label for="">CEP</label>

                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu cep";
                                  $atributos['id'] = "cep";
                                  echo form_input('cep', set_value('cep'), $atributos);
                                  echo form_error('cep', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>

                        <div class="col-md-3">

                            <div class="form-group">
                              <label for="">Endereço</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu endereço";
                                  $atributos['id'] = "endereco";
                                  echo form_input('endereco', set_value('endereco'), $atributos);
                                  echo form_error('endereco', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                              <label for="">Número</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o número";
                                  $atributos['id'] = "numero";
                                  echo form_input('numero', set_value('numero'), $atributos);
                                  echo form_error('numero', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="exampleInputPassword1">Complemento</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o complemento";
                                  $atributos['id'] = "numero";
                                  echo form_input('complemento', set_value('complemento'), $atributos);
                                  echo form_error('complemento', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Bairro</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o bairro";
                                  $atributos['id'] = "bairro";
                                  echo form_input('bairro', set_value('bairro'), $atributos);
                                  echo form_error('bairro', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                    </div>


                    <div class="row">


                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Estado</label>
 
                              <?php
                                $atributos = null;
                                $atributos['class'] = "form-control selecionar_cidade";
                                $atributos['id'] = "estado";
                                echo form_dropdown('estado', $estados, set_value('estado'), $atributos);
                                echo form_error('estado', '<div class="error">', '</div>');
                              ?>                            
                            </div>

                        </div>

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Cidade</label>
                              <?php
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $atributos['id'] = "cidade";
                                  echo form_dropdown('cidade', array('' => 'Selecione o Estado'), set_value('cidade'), $atributos);
                                  echo form_error('cidade', '<div class="error">', '</div>');
                                  echo form_hidden('callback_cidade', set_value('cidade'));
                              ?>    
                            </div>

                        </div>

                    </div>
                        <?php 
                        echo form_submit('btn', 'cadastrar');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    