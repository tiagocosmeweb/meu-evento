   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Enviar e-mail</h2>

                  <?php 
                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('eventos/enviar_email', $atributos);
                    echo form_hidden('evento_id', $evento_id);
                  ?>

                    <div class = "row">

                      <div class="col-md-12">

                          <div class="form-group">
                              <label for="">Assunto</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Assunto do e-mail";
                                  $atributos['id'] = "descricao";
                                  echo form_input('titulo', set_value('titulo'), $atributos);
                                  echo form_error('titulo', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>

                        <div class="col-md-12">

                          <div class="form-group">
                              <label for="">Mensagem</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Mensagem do e-mail";
                                  $atributos['id'] = "descricao";
                                  echo form_textarea('descricao', set_value('descricao'), $atributos);
                                  echo form_error('descricao', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>
                    </div>
    
                    </div>
                        <?php 
                        echo form_submit('btn', 'enviar mensagem');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    