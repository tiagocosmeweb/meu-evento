   <section id="search">
            <div class="container">
                <div class="col-sm-12" style = 'min-height: 350px;'>          
                
                 <h1 style = 'margin-bottom: 25px'> Participantes </h1>


                <?php

                $mensagem = $this->session->flashdata('mensagem');

                if($mensagem != null)
                {
                ?>

                        <span class = 'alert alert-success'>
                            <?= $this->session->flashdata('mensagem');  ?>
                        </span>

                <?php
                }
                ?>

                 <table class = "table table-bordered table-hover" style = 'margin-top: 25px'>
                 
                 <tr>
                 	<th> Nome </th>
                    <th> CPF </th>
                 	<th> Presença </th>
                 	<th> Crachá </th>
                    <th> Situação </th>
                 
                 </tr>

                <?php

                $atts = array(
                  'width'      => '400',
                  'height'     => '400',
                  'scrollbars' => 'yes',
                  'status'     => 'no',
                  'resizable'  => 'yes',
                  'screenx'    => '0',
                  'screeny'    => '0'
                );

                if(count($participantes) >= 1)
                {
                 	foreach($participantes as $participante)
                 	{

                ?>

                	<tr>
                 		
                        <td width='80%'> <?= $participante['participante'][0]->nomecompleto; ?></td>
                        <td width='80%'> <?= $participante['participante'][0]->cpf; ?></td>
                 	    
                        <td>               
                            <?= anchor('eventos/presenca/' . $evento_id . '/' . $participante['participante'][0]->id , "<i class = 'glyphicon glyphicon-user'></i>"); ?> 
                        </td>
                 	   
                        <td> 
                            <?php

                                if($participante['inscricao']->presenca == 1)
                                {
                                    echo anchor_popup('eventos/cracha/' . $evento_id . '/' .  $participante['participante'][0]->id , "<i class = 'glyphicon glyphicon-user'></i>", $atts) ;
                                }

                               else
                                echo  '---------'; 
                           ?>  
                        </td>

                        <td> 
                            <?= ($participante['inscricao']->presenca == 0) ? ' ---------' : 'Presente'; ?>
                        </td>

                 	</tr>
                <?php
            		}
             	}
                else
                {
                ?>
                    <p> Ainda não há participantes </p>
                <?php
                }

                ?>
                
                </table>
                
                </div>
        </div>     

    </section>
  