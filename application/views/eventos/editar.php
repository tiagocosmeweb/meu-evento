   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Editar evento</h2>

                  <?php 

                    //echo "<pre>" . print_r($evento, true) . "</pre>";
                    //echo "<pre>" . print_r($endereco, true) . "</pre>";

                    $atributos = null;
                    $atributos['id'] = 'form_evento'; 
                    echo form_open('eventos/salvar', $atributos); 
                    echo form_hidden('id', $evento[0]->id); 
                    echo form_hidden('endereco_id', $endereco[0]->id); 
                  ?>

                    <div class="row">
                      <div class="col-md-8">
 
                        <div class="form-group">
                             <label for="">Título do evento</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Título do evento";
                                $valor = (set_value('titulo')) ? set_value('titulo') : $evento[0]->titulo;
                                echo form_input('titulo', $valor, $atributos);
                                echo form_error('titulo', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>
                    </div>

                    <div class = "row">

                        <div class="col-md-8">

                          <div class="form-group">
                              <label for="">Descrição do evento</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Descreva o seu evento";
                                  $atributos['id'] = "descricao";
                                  $valor = (set_value('descricao')) ? set_value('descricao') : $evento[0]->descricao;
                                  echo form_textarea('descricao', $valor, $atributos);
                                  echo form_error('descricao', '<div class="error">', '</div>');
                                ?>
                          </div>
                        </div>
                    </div>

                      <div class="row">

                        <div class="col-md-3">

                            <div class="form-group">
                              
                              <label for="">Data inicial</label>

                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control formatacao_data_evento";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Data e hora inicial";
                                $valor = (set_value('data_inicio')) ? set_value('data_inicio') :
                                date('d/m/Y h:i:s', strtotime($evento[0]->data_inicio));
                                echo form_input('data_inicio', $valor, $atributos);
                                echo form_error('data_inicio', '<div class="error">', '</div>');
                              ?>

                        </div>

                        </div>

                          <div class="col-md-3">

                            <div class="form-group">
                             <label for="">Data final</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control formatacao_data_evento";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Data e hora final";
                                $valor = (set_value('data_final')) ? set_value('data_final') :
                                date('d/m/Y h:i:s', strtotime($evento[0]->data_final));
                                echo form_input('data_final', $valor, $atributos);
                                echo form_error('data_final', '<div class="error">', '</div>');
                              ?>

                        </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                             <label for="">Número de vagas</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "titulo";
                                $atributos['placeholder'] = "Número de vagas";
                                $valor = (set_value('numero_vaga')) ? set_value('numero_vaga') :
                                $evento[0]->numero_vaga;
                                echo form_input('numero_vaga', $valor, $atributos);
                                echo form_error('numero_vaga', '<div class="error">', '</div>');
                              ?>
                        </div>

                        </div>
                        </div>

                        <div class= "row">

                        <div class="col-md-4">

                            <div class="form-group">
                             <label for="">Evento gratuito ou pago?</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "tipo";
                                $atributos['placeholder'] = "Número de vagas";
                                
                                $tipo[''] = 'Selecione';
                                $tipo['0'] = 'Grátis';
                                $tipo['1'] = 'Pago';

                                $valor = (set_value('tipo')) ? set_value('tipo') :
                                $evento[0]->tipo;

                                echo form_dropdown('tipo',$tipo, $valor, $atributos);
                                echo form_hidden('callback_tipo', $valor);
                                echo form_error('tipo', '<div class="error">', '</div>');
                              ?>
                        </div>

                        </div>
                        </div>

                      <div class= "row">

                        <div class="col-md-4" id='ocultar_valor'>

                            <div class="form-group">
                             <label for="">Valor</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control valor";
                                $atributos['id']       = "valor";
                                $atributos['placeholder'] = "Valor";

                                $valor = (set_value('valor')) ? set_value('valor') :
                                $evento[0]->valor;

                                echo form_input('valor', $valor, $atributos);
                                echo form_error('valor', '<div class="error">', '</div>');
                              ?>
                        </div>


                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Situação</label>
                              <?php
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                      
                                  $situacao[''] = 'Selecione';
                                  $situacao['1'] = 'Publicado';
                                  $situacao['0'] = 'Não publicado';

                                  $valor = (set_value('situacao')) ? set_value('situacao') :
                                  $evento[0]->situacao;

                                  echo form_dropdown('situacao', $situacao, $valor, $atributos);
                                  echo form_error('situacao', '<div class="error">', '</div>');


                              ?>    
                            </div>
                        </div>

                        </div>


                    </div>

                    <h2> Localidade do evento </h2>

                      <div class="row">

                        <div class="col-md-8">
                              <label for="">Local do evento</label>

                            <div class="form-group">
                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Local (nome, hotel, instituição... )";
                                  $atributos['id'] = "local";
                                  $valor = (set_value('local')) ? set_value('local') :
                                  $endereco[0]->local;
                                  echo form_input('local', $valor, $atributos);
                                  echo form_error('local', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>
                      </div>

                      <div class="row">

                       <div class="col-md-3">

                            <div class="form-group">
                              <label for="">CEP</label>

                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu cep";
                                  $atributos['id'] = "cep";
                                  $valor = (set_value('cep')) ? set_value('cep') :
                                  $endereco[0]->cep;
                                  echo form_input('cep', $valor, $atributos);
                                  echo form_error('cep', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>

                        <div class="col-md-3">

                            <div class="form-group">
                              <label for="">Endereço</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu endereço";
                                  $atributos['id'] = "endereco";
                                  $valor = (set_value('endereco')) ? set_value('endereco') :
                                  $endereco[0]->endereco;
                                  echo form_input('endereco', $valor, $atributos);
                                  echo form_error('endereco', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                              <label for="">Número</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o número";
                                  $atributos['id'] = "numero";
                                  $valor = (set_value('numero')) ? set_value('numero') :
                                  $endereco[0]->numero;
                                  echo form_input('numero', $valor, $atributos);
                                  echo form_error('numero', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="exampleInputPassword1">Complemento</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o complemento";
                                  $atributos['id'] = "numero";
                                  $valor = (set_value('complemento')) ? set_value('complemento') :
                                  $endereco[0]->complemento;
                                  echo form_input('complemento', $valor, $atributos);
                                  echo form_error('complemento', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Bairro</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o bairro";
                                  $atributos['id'] = "bairro";
                                  $valor = (set_value('bairro')) ? set_value('bairro') :
                                  $endereco[0]->bairro;
                                  echo form_input('bairro', $valor, $atributos);
                                  echo form_error('bairro', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                    </div>


                    <div class="row">

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Estado</label>
 
                              <?php
                                $atributos = null;
                                $atributos['class'] = "form-control selecionar_cidade";
                                $atributos['id'] = "estado";
                                $valor = (set_value('estado')) ? set_value('estado') :
                                $endereco[0]->estado_id;
                                echo form_dropdown('estado', $estados, $valor, $atributos);
                                echo form_error('estado', '<div class="error">', '</div>');
                              ?>                            
                            </div>

                        </div>

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Cidade</label>
                              <?php
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $atributos['id'] = "cidade";
                                  echo form_dropdown('cidade', array('' => 'Selecione o Estado'), set_value('cidade'), $atributos);
                                  echo form_error('cidade', '<div class="error">', '</div>');

                                  $valor = (set_value('cidade')) ? set_value('cidade') :
                                  $endereco[0]->cidade_id;

                                  echo form_hidden('callback_cidade', $valor);
                              ?>    
                            </div>

                        </div>
                    </div>
                        <?php 
                        echo form_submit('btn', 'cadastrar');
                        echo form_close(); ?>
                      </div>     
  
                </div> 
                </div>
    </section>
    