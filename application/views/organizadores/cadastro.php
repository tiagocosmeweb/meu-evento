   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                
                <div class= "row">

                
                  <?php

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open('organizadores/salvar', $atributos); 

                    $data = array
                            (
                              'name'        => 'tipo_pessoa',
                              'class'          => 'tipo_pessoa',
                              'value'       => 'pj',
                              'checked'     => (set_value('tipo_pessoa') == 'pj') ? true : true,
                              'style'       => 'margin:10px',
                            );

                    echo form_radio($data) . 'Pessoa Jurídica';

                  ?>

                <div class= "row pj">

                    <h3> Pessoa jurídica </h3> 

                     <div class="row">

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Razão Social</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite a razão social";
                                  $atributos['id'] = "razaosocial";
                                  echo form_input('razaosocial', set_value('razaosocial'), $atributos);
                                  echo form_error('razaosocial', '<div class="error">', '</div>');
                                ?>                        
                            </div>

                        </div>

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">CNPJ</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite O CNPJ";
                                  $atributos['id'] = "cnpj";
                                  echo form_input('cnpj', set_value('cnpj'), $atributos);
                                  echo form_error('cnpj', '<div class="error">', '</div>');
                                ?>                        
                            </div>

                        </div>

                      </div>

                      <div class="row">


                        <div class="col-md-3">

                            <div class="form-group">
                            
                            <h3> Endereço </h3>

                            <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu cep";
                                  $atributos['id'] = "cep_empresa";
                                  echo form_input('cep_empresa', set_value('cep_empresa'), $atributos);
                                  echo form_error('cep_empresa', '<div class="error">', '</div>');
                                ?>    
                            </div>

                        </div>
                      </div>

                      <div class="row">

                        <div class="col-md-6">

                            <div class="form-group">
                              <label for="">Endereço</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite seu endereço";
                                  $atributos['id'] = "endereco_empresa";
                                  echo form_input('endereco_empresa', set_value('endereco_empresa'), $atributos);
                                  echo form_error('endereco_empresa', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="form-group">
                              <label for="">Número</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o número";
                                  $atributos['id'] = "numero_empresa";
                                  echo form_input('numero_empresa', set_value('numero_empresa'), $atributos);
                                  echo form_error('numero_empresa', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                      </div>

                      <div class="row">
                        
                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="exampleInputPassword1">Complemento</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o complemento";
                                  $atributos['id'] = "numero_empresa";
                                  echo form_input('complemento_empresa', set_value('complemento_empresa'), $atributos);
                                  echo form_error('complemento_empresa', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>


                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Bairro</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite o bairro";
                                  $atributos['id'] = "bairro_empresa";
                                  echo form_input('bairro_empresa', set_value('bairro_empresa'), $atributos);
                                  echo form_error('bairro_empresa', '<div class="error">', '</div>');
                                ?>  
                            </div>

                        </div>
                    </div>


                    <div class="row">


                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Estado</label>
 
                              <?php
                                $atributos = null;
                                $atributos['class'] = "form-control selecionar_cidade";
                                $atributos['id'] = "estado_empresa";
                                echo form_dropdown('estado_empresa', $estados, set_value('estado_empresa'), $atributos);
                                echo form_error('estado_empresa', '<div class="error">', '</div>');
                              ?>                            
                            </div>

                        </div>

                         <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Cidade</label>
                              <?php
                                  $atributos = null;
                                  $atributos['class'] = "form-control";
                                  $atributos['id'] = "cidade_empresa";
                                  echo form_dropdown('cidade_empresa', array('' => 'Selecione o Estado'), set_value('cidade_empresa'), $atributos);
                                  echo form_error('cidade_empresa', '<div class="error">', '</div>');
                                  echo form_hidden('callback_cidade_empresa', set_value('cidade_empresa'));
                              ?>    
                            </div>

                        </div>

                    </div>



                </div>
                
                <div class= "row">
                    
                  <h3> Dados pessoais </h3>


                    <div class="row">
                      <div class="col-md-4">
 
                        <div class="form-group">
                            <label for="">Nome completo</label>
                              <?php 
                                $atributos = null;
                                $atributos['class']    = "form-control";
                                $atributos['id']       = "id";
                                $atributos['placeholder'] = "Digite o seu nome";
                                echo form_input('nomecompleto', set_value('nomecompleto'), $atributos);
                                echo form_error('nomecompleto', '<div class="error">', '</div>');
                              ?>

                        </div>
                      </div>

                        <div class="col-md-2">

                          <div class="form-group">
                              <label for="">Data de nascimento</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua data de nascimento";
                                  $atributos['id'] = "datanascimento";
                                  echo form_input('datanascimento', set_value('datanascimento'), $atributos);
                                  echo form_error('datanascimento', '<div class="error">', '</div>');
                                ?>
                          </div>

                        </div>

                        <div class="col-md-2">

                          <div class="form-group">
                           <label for="">CPF</label>
                                <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu CPF";
                                  $atributos['id'] = "cpf";
                                  echo form_input('cpf', set_value('cpf'), $atributos);
                                  echo form_error('cpf', '<div class="error">', '</div>');
                                ?>
                          </div>
                      </div>
                    </div>

                      <div class="row">

                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="">Área de atuação</label>
                            
                            <?php
                              $atributos = null;
                              $atributos['class'] = "form-control";
                              echo form_dropdown('areaatuacao', $areaatuacao, set_value('areaatuacao'), $atributos);
                              echo form_error('areaatuacao', '<div class="error">', '</div>');
                            ?>

                          </div>

                        </div>

                        <div class="col-md-4">

                          <div class="form-group">
                            <label for="">Nível</label>
                            
                            <?php
                              $atributos = null;
                              $atributos['class'] = "form-control";
                              echo form_dropdown('nivel',$nivel, set_value('nivel') , $atributos);
                              echo form_error('nivel', '<div class="error">', '</div>');
                            ?>
                          </div>
                        </div>
                    </div>

                      <h3> Dados de acesso </h3>

                      <div class="row">

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">E-mail</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu E-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>                        
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Senha</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua senha";
                                  echo form_password('senha', set_value('senha'), $atributos);
                                  echo form_error('senha', '<div class="error">', '</div>');
                                ?>      
                            </div>

                        </div>

                      </div>     

                </div>

              <?php 
                    echo form_submit('btn', 'cadastrar');
                    echo form_close(); 
              ?>
            </div>
        </div>     

    </section>
  