   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                
                <div class= "row">

                
                  <?php

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 

                    if($mensagem != null)
                    {
                        echo $mensagem;
                    } 

                    echo form_open('organizadores/logar', $atributos); 

                  ?>
                
                <div class= "row">
                    
                      <h3> Dados de acesso </h3>

                      <div class="row">

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">E-mail</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu E-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>                        
                            </div>

                        </div>

                        <div class="col-md-4">

                            <div class="form-group">
                              <label for="">Senha</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "Digite sua senha";
                                  echo form_password('senha', set_value('senha'), $atributos);
                                  echo form_error('senha', '<div class="error">', '</div>');
                                ?>      
                            </div>

                        </div>

                      </div>     

                </div>

              <?php 
                    echo form_submit('btn', 'cadastrar');
                    echo form_close(); 
              ?>
            </div>
        </div>     

    </section>
  