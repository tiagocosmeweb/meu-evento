   <section id="search">
            <div class="container">
                <div class="col-sm-12">
                <div class= "row">
                    
                  <h2>Login</h2>

                  <?php 
                    switch ($mensagem) 
                    {
                      case 'senha-invalida':
                        $msg = 'Senha inválida';
                        break;

                      case 'credencial-nao-encontrada':
                        $msg = 'Crendencial não encontrada';
                        break;

                      case 'cadastro-nao-confirmado':
                        $msg = 'A conta não encontra-se ativada. 
                        Confirme através do e-mail que enviamos para você.';
                        break;

                      case 'permissao-negada':
                        $msg = 'Acesso não autorizado. Tente novamente.';
                        break;
                    }

                    if($cadastro == 'participante')
                    {
                        echo anchor('participantes', 'Não tem cadastro? clique aqui', 'class= "btn"');
                    }

                    if($cadastro == 'organizador')
                    {
                        echo anchor('organizadores', 'Não tem cadastro? clique aqui', 'class= "btn"');
                    }


                    if($mensagem != null)
                    {
                      echo "<p class = 'alert alert-danger'>" . $msg. "</p>";
                    } 

                    $atributos = null;
                    $atributos['id'] = 'form_cadastro'; 
                    echo form_open('usuarios/logar', $atributos); 
                  ?>

                    <div class="row">
                      <div class="col-md-4">
 
                        <div class="form-group">
                           <label for="">E-mail</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite seu e-mail";
                                  $atributos['id'] = "email";
                                  echo form_input('email', set_value('email'), $atributos);
                                  echo form_error('email', '<div class="error">', '</div>');
                                ?>       
                        </div>


                        <div class="form-group">
                           <label for="">Senha</label>
                               <?php 
                                  $atributos = null;
                                  $atributos['class']       = "form-control";
                                  $atributos['placeholder'] = "digite sua senha";
                                  $atributos['id'] = "senha";
                                  echo form_password('senha', set_value('senha'), $atributos);
                                  echo form_error('senha', '<div class="error">', '</div>');
                                ?>       
                        </div>
 
                        <div class="form-group">
                           <label for="">Meu cadastro é: </label>
                            <?php

                            switch ($cadastro) {
                              case 'participante':
                                $valor = 'participante';
                                break;

                              case 'organizador':
                                $valor = 'organizador';
                                break;
                              
                              default:
                                $valor = set_value('tipocadastro');
                                break;
                            }

                               $atributos = null;
                               $atributos['class']       = "form-control";
                               $tipocadastro = array('' => 'Selecione', 'participante' => 'Participante', 'organizador' => 'Organizador');
                               echo form_dropdown('tipocadastro', $tipocadastro, $valor, $atributos);
                               echo form_error('tipocadastro', '<div class="error">', '</div>');

                            ?>
                        </div>

                        <div>
                          <?php 
                          echo form_submit('btn', 'logar');
                          echo form_close(); 
                          ?>
                      </div>  

                      </div>
               
                  </div> 
                </div>
              </div>
            </div> 
    </section><!--/#twitter-feed-->
    