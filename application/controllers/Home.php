<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
	public function index()
	{

		$this->load->model('Evento_model', 'evento');
		
		$data['fila1_eventos'] = $this->evento->ultimos(0);
		$data['fila2_eventos'] = $this->evento->ultimos(3);

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu.php');
		$this->load->view('home/home.php');
		//$this->load->view('home/buscar.php');
		$this->load->view('home/eventos.php', $data);
		$this->load->view('home/sobre.php');
		// $this->load->view('home/contato.php');
		$this->load->view('home/rodape.php');

	}
}

?>