<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('funcoes');
		$this->load->library('sessao');
		$this->load->library('emailautomatico');

	}

	public function gerenciar()
	{
		$this->sessao->consultar('id');

		$organizador['organizador_id'] = $this->sessao->get('id');

		$this->load->model('Evento_model', 'evento');
		$ultimos_eventos = $this->evento->condicoes($organizador);
		$data['eventos'] = $ultimos_eventos;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/gerenciar.php', $data);
		$this->load->view('home/rodape.php');

	}

	public function criar()
	{
		$this->sessao->consultar('id');

		$this->load->model('Estado_model', 'estado');
		$estados = $this->estado->selecionar();

		$cadastro['estados'] = $this->funcoes->converter_array_form($estados, 'uf');

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/criar.php', $cadastro);
		$this->load->view('home/rodape.php');

	}


	public function editar($evento_id)
	{
		$this->sessao->consultar('id');

		$this->load->model('Estado_model', 'estado');
		$estados = $this->estado->selecionar();

		$cadastro['estados'] = $this->funcoes->converter_array_form($estados, 'uf');

		$this->load->model('Evento_model', 'evento');
		$cadastro['evento'] = $this->evento->get((int) $evento_id);

		$this->load->model('Enderecoevento_model', 'endereco');
		$cadastro['endereco'] = $this->endereco->condicoes(array('evento_id' => $evento_id));

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/editar.php', $cadastro);
		$this->load->view('home/rodape.php');

	}


	public function salvar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			
			$this->form_validation->set_rules('titulo', 'título', 'trim|required');
			$this->form_validation->set_rules('descricao', 'descricao', 'trim|required');
			$this->form_validation->set_rules('data_inicio', 'data de início', 'trim|required');
			$this->form_validation->set_rules('data_final', 'data de finalização', 'trim|required');
			$this->form_validation->set_rules('numero_vaga', 'numero_vaga', 'trim|required');
			$tipo = $this->input->post('tipo');

			if($tipo == '1')
			{
				$this->form_validation->set_rules('valor', 'valor', 'trim|required');
			}
			
			$this->form_validation->set_rules('tipo', 'tipo', 'trim|required');
			$this->form_validation->set_rules('local', 'local', 'trim|required');
			$this->form_validation->set_rules('cep', 'cep', 'trim|required');
			$this->form_validation->set_rules('endereco', 'endereco', 'trim|required');
			$this->form_validation->set_rules('numero', 'número', 'trim|required');
			$this->form_validation->set_rules('bairro', 'bairro', 'trim|required');
			$this->form_validation->set_rules('estado', 'estado', 'trim|required');
			$this->form_validation->set_rules('cidade', 'cidade', 'trim|required');

			if(!$this->form_validation->run())
			{
				if(!array_key_exists('id', $this->input->post()))
				{	
					$this->criar();
				}
				else
				{
					$this->editar($this->input->post('id'));
				}
			}

			else
			{

				$evento = null;
				$evento['titulo']    = $this->input->post('titulo');
				$evento['descricao'] = $this->input->post('descricao');
				$evento['data_inicio'] = $this->funcoes->formata_data_evento($this->input->post('data_inicio'));
				$evento['data_final'] = $this->funcoes->formata_data_evento($this->input->post('data_final'));
				$evento['numero_vaga'] = $this->input->post('numero_vaga');
				$evento['tipo'] = $this->input->post('tipo');
				$evento['organizador_id'] = $this->sessao->get('id');

				if($tipo == '1')
				{
					$valor = str_replace('.', '',  $this->input->post('valor'));
					$valor = str_replace(',', '.',  $valor);

					$evento['valor'] = str_replace(',', '.', $valor);
				}

				$this->load->model('Evento_model', 'evento');
				$this->load->model('Enderecoevento_model', 'endereco_evento');

				$endereco = null;
				$endereco['local'] = $this->input->post('local');
				$endereco['endereco'] = $this->input->post('endereco');
				$endereco['numero'] = $this->input->post('numero');
				$endereco['complemento'] = $this->input->post('complemento');
				$endereco['bairro'] = $this->input->post('bairro');
				$endereco['estado_id'] = $this->input->post('estado');
				$endereco['cidade_id'] = $this->input->post('cidade');
				$endereco['cep'] = $this->input->post('cep');

				if(!array_key_exists('id', $this->input->post()))
				{	
					$id_evento = $this->evento->salvar($evento);


					if(is_numeric($id_evento))
					{		
						$endereco['evento_id'] = $id_evento;
	
						$this->endereco_evento->salvar($endereco);
						$this->session->set_flashdata('mensagem', 'Evento cadastrado com sucesso.');
						redirect(base_url('/eventos/gerenciar'));
					}

					else
						redirect(base_url('/usuarios/login'));
				}

				else
				{
					$evento['situacao'] = $this->input->post('situacao');
					$this->evento->salvar($evento, $acao = 'alterar', $this->input->post('id'));

					$endereco['evento_id'] = $this->input->post('id');
					$this->endereco_evento->salvar($endereco, $acao = 'alterar', $this->input->post('endereco_id'));
					
					$this->session->set_flashdata('mensagem', 'Evento alterado com sucesso.');
					redirect(base_url('/eventos/gerenciar'));

				}

					
				if(is_numeric($id_evento))
				{			
					
					$endereco = null;
					$endereco['local'] = $this->input->post('local');
					$endereco['endereco'] = $this->input->post('endereco');
					$endereco['numero'] = $this->input->post('numero');
					$endereco['complemento'] = $this->input->post('complemento');
					$endereco['bairro'] = $this->input->post('bairro');
					$endereco['estado_id'] = $this->input->post('estado');
					$endereco['cidade_id'] = $this->input->post('cidade');
					$endereco['cep'] = $this->input->post('cep');
					$endereco['evento_id'] = $id_evento;

					$this->load->model('Enderecoevento_model', 'endereco_evento');
					$this->endereco_evento->salvar($endereco);
					$this->session->set_flashdata('mensagem', 'Evento cadastrado com sucesso.');
					redirect(base_url('/eventos/gerenciar'));

				}
				else
					redirect(base_url('/usuarios/login'));
			}
		}
	}

	public function detalhes($evento_id)
	{
		$this->load->model('Evento_model', 'evento');
		$evento = $this->evento->get((int) $evento_id);

		if(count($evento) == 0)
		{
			$this->load->view('layout/nao_disponivel');
		}

		else if($evento['0']->situacao == 0)
		{
			$this->load->view('layout/nao_disponivel');
		}

		else
		{

			$this->load->model('Enderecoevento_model', 'endereco_evento');

			$condicoes = null;
			$condicoes['evento_id'] = $evento[0]->id;
			$endereco = $this->endereco_evento->condicoes($condicoes);

			$this->load->model('Cidade_model', 'cidade');
			$cidade = $this->cidade->get((int) $endereco[0]->cidade_id);

			$this->load->model('Estado_model', 'estado');
			$estado = $this->estado->get((int) $endereco[0]->estado_id);

			$condicoes = null;
			$condicoes['evento_id'] = $evento[0]->id;

			$this->load->model('Palestrante_model', 'palestrante');
			$palestrantes = $this->palestrante->condicoes($condicoes);

			$this->load->model('Conteudo_model', 'conteudo');
			$conteudos = $this->conteudo->condicoes($condicoes);

			$this->load->model('Video_model', 'video');
			$videos = $this->video->condicoes($condicoes);

			$this->load->model('Faq_model', 'faq');

			$condicoes_faq['evento_id'] = $evento[0]->id;
			$condicoes_faq['situacao'] = 1;
			$faq = $this->faq->condicoes($condicoes_faq);
			$data['faq'] = $faq;

			$this->load->model('Inscricao_model', 'inscricao');

			$condicoes = null;
			$condicoes['evento_id'] = $evento[0]->id;
			$condicoes['situacao'] = 1;

			$total_inscritos = $this->inscricao->total($condicoes);
			$data['total_inscritos'] = $total_inscritos;
			
			// armazena qual o último evento o participante visualizou para futura inscrição.

			$sessao['evento_atual'] = $evento_id;
			$this->sessao->criar($sessao);

			$data['evento'] = $evento;
			$data['endereco'] = $endereco;
			$data['cidade'] = $cidade;
			$data['estado'] = $estado;

			$data['conteudos'] = $conteudos;
			$data['videos'] = $videos;
			$data['palestrantes'] = $palestrantes;

			$this->load->view('layout/index.php', $data);

		}

	}

	public function email($evento_id)
	{
		$data['evento_id'] = $evento_id;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/email.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function enviar_email()
	{

		$this->form_validation->set_rules('titulo', 'título', 'trim|required');
		$this->form_validation->set_rules('descricao', 'descrição', 'trim|required');
		
		if(!$this->form_validation->run())
		{
			$this->email($this->input->post('evento_id'));
		}
		else
		{
			$inscricao = null;
			$inscricao['evento_id'] = $this->input->post('evento_id');
			$inscricao['situacao'] = 1;

			$this->load->model('Inscricao_model', 'inscricao');
			$this->load->model('Participante_model', 'participante');

			$participantes = $this->inscricao->condicoes($inscricao, $campos = 'participante_id');

			if(count($participantes) >= 1)
			{
				$emails = null;
				$emails = array();

				foreach($participantes as $participante)
				{
					$cadastro = $this->participante->get((int) $participante->participante_id, $campos = 'nomecompleto, email');
					$emails[] = $cadastro[0]->email;
				}

				$emails = implode(',', $emails);

				$assunto = $this->input->post('titulo');
				$mensagem = $this->input->post('descricao');

				$this->emailautomatico->enviar_email($assunto, $mensagem, $emails);
				$this->session->set_flashdata('mensagem', 'E-mail enviado com sucesso.');
			}

			else
			{
				$this->session->set_flashdata('mensagem', 'Não há participantes cadastrados no evento desejado no momento.');
			}

			redirect(base_url('/eventos/gerenciar'));

		}
		
	}

	public function participantes($evento_id)
	{

		$inscricao = null;
		$inscricao['evento_id'] = $evento_id;
		$inscricao['situacao']  = 1;

		$this->load->model('Inscricao_model', 'inscricao');

		$participantes = $this->inscricao->condicoes($inscricao, $campos = 'participante_id, presenca');

		if(count($participantes) >= 1)
		{
			$cadastro = null;
			$cadastro = array();

			$this->load->model('Participante_model', 'participante');

			$i = 0;

			foreach($participantes as $participante)
			{
				$cadastro[$i]['participante'] = $this->participante->get((int) $participante->participante_id, $campos = 'id, nomecompleto, email, cpf');
				$cadastro[$i]['inscricao'] = $participante;
				$i++;
			}


			// echo "<pre>" . print_r($cadastro, true). "</pre>";

			$participantes = $cadastro;

		}

		else
		{
			$participantes = null;
		}

		$data['participantes'] = $participantes;
		$data['evento_id'] = $evento_id;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/participantes.php', $data);
		$this->load->view('home/rodape.php');

	}

	public function presenca($evento_id, $participante_id)
	{
		$data['evento_id'] = $evento_id;
		$data['participante_id'] = $participante_id;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('eventos/presenca.php', $data);
		$this->load->view('home/rodape.php');
	}

	function salvar_presenca()
	{

		$this->form_validation->set_rules('evento_id', 'evento', 'trim|required');
		$this->form_validation->set_rules('participante_id', 'participante', 'trim|required');
		$this->form_validation->set_rules('presenca', 'status', 'trim|required');
		
		if(!$this->form_validation->run())
		{
			$this->presenca($this->input->post('evento_id'), $this->input->post('participante_id'));
		}
		else
		{
			$this->load->model('Inscricao_model', 'inscricao');

			$condicoes = null;
			$condicoes['participante_id'] = $this->input->post('participante_id');
			$condicoes['evento_id'] = $this->input->post('evento_id');
			$condicoes['situacao'] = 1;

			$inscricao = $this->inscricao->condicoes($condicoes);

			if(count($inscricao) == 1)
			{
				$id = $inscricao[0]->id;
				
				$data = null;
				$data['presenca'] = $this->input->post('presenca');
				$this->inscricao->salvar($data, $tipo = 'alterar', $id);
				$this->session->set_flashdata('mensagem', 'Confirmação de presença realizada com sucesso.');
			}

			else
			{
				$this->session->set_flashdata('mensagem', 'Ops, inscrição não encontrada.');
			}

			redirect('eventos/participantes/' . $this->input->post('evento_id'));
		}

	}

	public function cracha($evento_id, $participante_id)
	{

		$condicoes = null;
		$condicoes['participante_id'] = $participante_id;
		$condicoes['evento_id'] = $evento_id;
		$condicoes['presenca'] = 1;
		$condicoes['situacao'] = 1;

		$this->load->model('Evento_model', 'evento');
		$this->load->model('Participante_model', 'participante');
		$this->load->model('Inscricao_model', 'inscricao');

		$inscricao = $this->inscricao->condicoes($condicoes);

		if(count($inscricao) == 1)
		{
			if($inscricao[0]->situacao == 1)
			{
				$credencial['participante'] = $this->participante->get((int) $inscricao[0]->participante_id, $campos = ' nomecompleto,cpf');
				
				$credencial['evento'] = $this->evento->get((int) $inscricao[0]->evento_id);
			}
			else
				$credencial = null;
		}
		else
			$credencial = null;

		$data['credencial'] = $credencial;
		$this->load->view('eventos/cracha.php', $data);

	}

	public function relatorios($evento_id)
	{
		$this->load->model('Evento_model', 'evento');

		$data = array();

		$relatorio = $this->input->get('relatorio');

		// Carregamos a library PHPlot
		$this->load->library('PHPlot');

		$plot = new PHPlot(800,600);
		$plot->SetImageBorderType('plain');

		$plot->SetPlotType('pie');
		$plot->SetDataType('text-data-single');

		if($relatorio == 'sexo')
		{

			foreach($this->evento->relatorio_por_sexo($evento_id) as $aux)
			{
				// echo "<pre>" . print_r($aux, true) . "</pre>";

				if(($aux['sexo'] == 'F') and ($aux['inscricao'] == 0))
				{
					$data[] = array('Feminino ['.  $aux["nome"]  . '] - cancelados', $aux['total']);
				}

				if(($aux['sexo'] == 'F') and ($aux['inscricao'] == 1))
				{
					$data[] = array('Feminino [' . $aux["nome"] .'] - confirmadas', $aux['total']);
				}

				if(($aux['sexo'] == 'M') and ($aux['inscricao'] == 1))
				{
					$data[] = array('Masculino [' . $aux["nome"] .']- confirmados', $aux['total']);
				}

				if(($aux['sexo'] == 'M') and ($aux['inscricao'] == 0))
				{
					$data[] = array('Masculino [' . $aux["nome"] .']- cancelados', $aux['total']);
				}

			}

			$plot->SetDataValues($data);

			# Set enough different colors;
			// $plot->SetDataColors(array('pink', 'yellow', 'green', 'blue'));

			# Main plot title:
			$plot->SetTitle("Relatorio");

			# Build a legend from our data array.
			# Each call to SetLegend makes one line as "label: value".
			foreach ($data as $row)
			  $plot->SetLegend(implode(': ', $row));
			
			# Place the legend in the upper left corner:
			$plot->SetLegendPixels(5, 5);
		}
		else
		{
			exit('relatorio não encontrado');
		}

		$plot->DrawGraph();
	}

	public function sair()
	{
		$this->sessao->apagar();
		redirect('usuarios/mensagem/sair');
	}


}

?>