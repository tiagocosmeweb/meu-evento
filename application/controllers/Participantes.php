<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Participantes extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
	}

	public function index()
	{

		$this->load->helper('funcoes');

		$this->load->model('Areaatuacao_model', 'areaatuacao');
		$areas = $this->areaatuacao->selecionar();

		$this->load->model('Nivel_model', 'nivelprofissional');
		$niveis = $this->nivelprofissional->selecionar();

		$this->load->model('Estado_model', 'estado');
		$estados = $this->estado->selecionar();

		$cadastro['areaatuacao'] = $this->funcoes->converter_array_form($areas, 'area');
		$cadastro['nivel'] = $this->funcoes->converter_array_form($niveis, 'nivel');
		$cadastro['estados'] = $this->funcoes->converter_array_form($estados, 'uf');

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu.php');
		$this->load->view('home/home.php');
		$this->load->view('participantes/cadastro.php', $cadastro);
		$this->load->view('home/rodape.php');

	}

	public function salvar()
	{
		if($this->input->post())
		{
			
			$this->form_validation->set_rules('nomecompleto', 'nome', 'trim|required');
			$this->form_validation->set_rules('datanascimento', 'data de nascimento', 'trim|required|callback_validar_data_nascimento');
			$this->form_validation->set_rules('cpf', 'cpf', 'trim|required|is_unique[participante.cpf]');
			$this->form_validation->set_rules('areaatuacao', 'área de atuação', 'trim|required');
			$this->form_validation->set_rules('nivel', 'nivel', 'trim|required');
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[participante.email]');
			$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('endereco', 'endereco', 'trim|required');
			$this->form_validation->set_rules('numero', 'número', 'trim|required');
			$this->form_validation->set_rules('bairro', 'bairro', 'trim|required');
			$this->form_validation->set_rules('estado', 'estado', 'trim|required');
			$this->form_validation->set_rules('cidade', 'cidade', 'trim|required');

			if(!$this->form_validation->run())
			{
				$this->index();
			}

			else
			{

				$participante = null;
				$data_ativacao = date('Y-m-d h:i:s');

				$participante['nomecompleto'] = $this->input->post('nomecompleto');
				$participante['datanascimento'] = $this->funcoes->data_americana($this->input->post('datanascimento'));
				$participante['cpf'] = $this->input->post('cpf');
				$participante['area_id'] = $this->input->post('areaatuacao');
				$participante['nivel_id'] = $this->input->post('nivel');
				$participante['email'] = $this->input->post('email');
				$participante['senha'] = $this->encrypt->encode($this->input->post('senha'));
				$participante['datacadastro'] = $data_ativacao;

				$this->load->model('Participante_model', 'participante');
				$id_participante = $this->participante->salvar($participante);

				if(is_numeric($id_participante))
				{			
					
					$endereco = null;
					$endereco['endereco'] = $this->input->post('endereco');
					$endereco['numero'] = $this->input->post('numero');
					$endereco['complemento'] = $this->input->post('complemento');
					$endereco['bairro'] = $this->input->post('bairro');
					$endereco['estado_id'] = $this->input->post('estado');
					$endereco['cidade_id'] = $this->input->post('cidade');
					$endereco['cep'] = $this->input->post('cep');
					$endereco['participante_id'] = $id_participante;

					$this->load->model('Endereco_model', 'endereco');
					$this->endereco->salvar($endereco);

					$primeiroNome = $this->funcoes->primeiro_nome($participante['nomecompleto']);
					$email = $this->input->post('email');
					$chave_ativacao = $this->funcoes->gerar_token_ativacao();

					$this->load->model('Token_model', 'token');

					$token['participante_id'] = $id_participante;
					$token['token'] = $chave_ativacao;
					$token['datacadastro'] = $data_ativacao;
					$token['dataativacao'] = $data_ativacao;

					$this->token->salvar($token);
					$this->emailautomatico->boas_vindas($primeiroNome, $email, $chave_ativacao);
					unset($_POST);
					redirect('usuarios/mensagem/cadastro-sucesso');
				}

					/* $this->load->library('unit_test');
					$this->unit->run($id_participante, 'is_numeric', 'Teste de retorno de id da base de dados', $nota = 'Garante que a operação foi realizada no banco de dados com sucesso através do id de retorno na nova linha inserida.'); 						
					var_dump($this->unit->report());
					*/
		
			}

		}
		else
			redirect(base_url('/participantes'));
	}

	function validar_data_nascimento($string)
	{

		$data = explode('/', $string);

		if(checkdate ($data[1] , $data[0] , $data[2]))
		{
			return true;	
		}

		else
		{
			$this->form_validation->set_message('validar_data_nascimento', 'Digite uma data de nascimento válida');
			return FALSE;
		}

	}

	public function esqueci_senha()
	{
		$encerrar = false;

		if(@$this->input->post())
		{
			$encerrar = true;
		
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

			if(!$this->form_validation->run())
			{
				unset($_POST);
				$this->esqueci_senha();
			}

			else
			{
				$participante['email'] = $this->input->post('email');
			
				$this->load->model('Participante_model', 'participante');
				$participante = $this->participante->get($participante);

				if(count($participante) == 1)
				{

					$id = $participante[0]->id;
					$primeiroNome = $this->funcoes->primeiro_nome($participante[0]->nomecompleto);
					$email = $participante[0]->email;
					$chave_ativacao = $this->funcoes->gerar_token_ativacao();

					$this->load->model('Token_model', 'token');

					$data_ativacao = date('Y-m-d h:i:s');

					$token['participante_id'] = $id;
					$token['token'] = $chave_ativacao;
					$token['datacadastro'] = $data_ativacao;
					$token['dataativacao'] = $data_ativacao;

					$this->token->salvar($token);
					$this->emailautomatico->alterar_senha($primeiroNome, $email, $chave_ativacao);
					$this->mensagem($mensagem = 'email-alterar-senha');

				}

				else
				{
					$this->mensagem($mensagem = 'email-nao-encontrado');
				}
			}

		}
		if($encerrar == false)
		{
			$this->load->view('home/cabecalho.php');
			$this->load->view('home/menu_evento_login.php');
			$this->load->view('home/home.php');
			$this->load->view('participantes/esqueci_senha.php');
			$this->load->view('home/rodape.php');
		}

	}

	public function nova_senha($email, $chave_ativacao)
	{

	   $this->load->model('Token_model', 'token');
	   $validacao = $this->token->ativador($email, $chave_ativacao);

	    if(count($validacao) == 1)
	    {   
	    	$data['email'] = $email;
	    	$data['chave_ativacao'] = $chave_ativacao;

	    	$this->load->view('home/cabecalho.php');
		    $this->load->view('home/menu.php');
		    $this->load->view('home/home.php');
		    $this->load->view('participantes/nova_senha.php', $data);
		    $this->load->view('home/rodape.php');
	     
	    }
	    
	    else
	    {
			$this->mensagem($mensagem = 'email-chave-invalida');
	    }

	}

	public function salvar_senha()
	{

		if($this->input->post())
		{
			$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('email', 'email', '');
			$this->form_validation->set_rules('chave_ativacao', 'chave de ativação');

			if(!$this->form_validation->run())
			{
				$this->nova_senha();
			}

			else
			{
				$email = $this->input->post('email');
				$chave_ativacao = $this->input->post('chave_ativacao');
				$nova_senha = $this->encrypt->encode($this->input->post('senha'));

				$this->load->model('Token_model', 'token');
	   			$validacao = $this->token->ativador($email, $chave_ativacao);

	   			if(count($validacao) == 1)
	   			{
					$participante['senha'] = $nova_senha;

					$this->load->model('Participante_model', 'participante');
					$this->participante->salvar($participante, $tipo = 'editar', $validacao[0]->idparticipante);

					$this->token->apagar($validacao[0]->idtoken);

					$this->mensagem($mensagem = 'senha-alterada-sucesso');
	   			}


			}
		}

		else
		{
			redirect('/');
		}

	}

	public function painel()
	{		
		$this->sessao->consultar('id');
		$evento_selecionado = $this->sessao->get();

		$data['evento_selecionado'] = null;

		if(array_key_exists('evento_atual', $evento_selecionado))
		{
			$this->load->model('Evento_model', 'evento');
			$evento_selecionado = $this->evento->get((int)  $evento_selecionado['evento_atual']);

			$data['evento_selecionado'] = $evento_selecionado;
		}

		$this->load->model('Inscricao_model', 'inscricao');

		$condicoes = null;
		$condicoes['participante_id'] = $this->sessao->get('id');
		$inscricoes = $this->inscricao->condicoes($condicoes);

		if(count($inscricoes)>= 1)
		{

			$this->load->model('Evento_model', 'evento');

			$i = 0;

			foreach($inscricoes as $inscricao)
			{
				$evento = $this->evento->get((int) $inscricao->evento_id, 'titulo');
				$auxiliar_inscricao[$i]['evento'] = $evento;
				$auxiliar_inscricao[$i]['inscricao'] = $inscricao;
				$i++;
			}

			$inscricoes = $auxiliar_inscricao;
		}

		else
		{
			$inscricoes = null;
		}

		$data['inscricoes'] = $inscricoes;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login_participante.php');
		$this->load->view('home/home.php');
		$this->load->view('participantes/painel.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function inscricao()
	{
		$this->sessao->consultar('id');
		$evento_selecionado = $this->sessao->get();

		if(array_key_exists('id', $evento_selecionado) and array_key_exists('evento_atual', $evento_selecionado))
		{
			
			$inscricao = null;
			$inscricao['participante_id'] = $this->sessao->get('id');
			$inscricao['evento_id'] = $this->sessao->get('evento_atual');
			$inscricao['situacao'] = 1;

			$this->load->model('Inscricao_model', 'inscricao');

			$consulta_inscricao = $this->inscricao->condicoes($inscricao);

			if(count($consulta_inscricao) >= 1)
			{
				$this->session->set_flashdata('mensagem', 'OPS, sua inscrição já está garantida!');
			}

			else
			{
				$this->inscricao->salvar($inscricao);
				$this->session->set_flashdata('mensagem', 'Inscrição realizada com sucesso.');
			}

			//  delete sessão
			unset($_SESSION['evento_atual']);
			redirect(base_url('/participantes/painel'));

		}

		else
		{
			redirect('participantes/painel');
		}

	}

	public function cancelar($evento_id)
	{

		$inscricao = null;
		$inscricao['participante_id'] = $this->sessao->get('id');
		$inscricao['evento_id'] = $evento_id;
		$inscricao['situacao'] = 1;

		$this->load->model('Inscricao_model', 'inscricao');
		$consulta_inscricao = $this->inscricao->condicoes($inscricao);

		if(count($consulta_inscricao) == 1)
		{
			$inscricao = null;
			$inscricao['situacao'] = 0;

			$this->inscricao->salvar($inscricao, $acao = 'alterar', $consulta_inscricao[0]->id);
			$this->session->set_flashdata('mensagem', 'Cancelamento realizado com sucesso.');
		}
		else
		{
			$this->session->set_flashdata('mensagem', 'Inscrição não encontrada ou já cancelada.');
		}

		redirect(base_url('/participantes/painel'));

	}

}

?>