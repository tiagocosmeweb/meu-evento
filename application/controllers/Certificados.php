<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Certificados extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('funcoes');
		$this->load->library('sessao');
		$this->load->library('emailautomatico');

	}

	public function gerar($evento_id)
	{
		// participantes ativos

		$condicoes = null;
		$condicoes['evento_id'] = $evento_id;
		$condicoes['presenca'] = 1;
		$condicoes['situacao'] = 1;

		$this->load->model('Inscricao_model', 'inscricao');
		$inscricoes = $this->inscricao->condicoes($condicoes);

		$this->load->model('Certificado_model', 'certificado');

		if(count($inscricoes) >= 1)
		{

			$this->load->model('participante_model', 'participante');

			foreach($inscricoes as $inscricao)
			{

				$participante = $this->participante->get((int) $inscricao->participante_id, $campos = 'id, nomecompleto, email');
				
				// gerar chave de acesso ao certificado

				$certificado = null;
				$certificado['chave'] = date('dmyhis') . rand(0, 1000); 
				$certificado['tipo'] = 'participante'; 
				$certificado['evento_id'] = $evento_id; 
				$certificado['participante_id'] = $participante[0]->id; 
				$this->certificado->salvar($certificado);

				$primeiroNome = $this->funcoes->primeiro_nome($participante[0]->nomecompleto);
				$email = $participante[0]->email;
				$link_certificado = $certificado['chave'];

				$this->emailautomatico->certificado($primeiroNome, $email, $link_certificado);

			}

		}
		
		// palestrantes

		$condicoes = null;
		$condicoes['evento_id'] = $evento_id;

		$this->load->model('Palestrante_model', 'palestrante');
		$palestrantes = $this->palestrante->condicoes($condicoes, $campos = 'id, nome, email');

		if(count($palestrantes) >= 1)
		{
			foreach($palestrantes as $palestrante)
			{
				// gerar chave de acesso ao certificado

				$certificado = null;
				$certificado['chave'] = date('dmyhis') . rand(0, 1000); 
				$certificado['tipo'] = 'palestrante'; 
				$certificado['evento_id'] = $evento_id; 
				$certificado['palestrante_id'] = $palestrante->id; 
				$this->certificado->salvar($certificado);

				$primeiroNome = $this->funcoes->primeiro_nome($palestrante->nome);
				$email = $palestrante->email;
				$link_certificado = $certificado['chave'];

				$this->emailautomatico->certificado($primeiroNome, $email, $link_certificado);
			}
		}

		$this->session->set_flashdata('mensagem', 'Certificado enviado com sucesso.');
		redirect(base_url('/eventos/gerenciar'));
	}

	function online($chave)
	{
		$this->load->model('Certificado_model', 'certificado');

		$data['chave'] = $chave;
		$verificar_chave = $this->certificado->condicoes($data);

		if(count($verificar_chave) == 1)
		{
			// palestrante

			if($verificar_chave[0]->tipo == 'palestrante')
			{
				$this->load->model('Palestrante_model', 'palestrante');
				$palestrante = $this->palestrante->get((int) $verificar_chave[0]->palestrante_id, $campos = 'nome');
				$nome = $palestrante[0]->nome;

				$this->load->model('Evento_model', 'evento');
				$evento = $this->evento->get((int) $verificar_chave[0]->evento_id, $campos = 'titulo, data_inicio');
				$titulo = $evento[0]->titulo;
				$data_inicio =  date('d/m/Y', strtotime($evento[0]->data_inicio));

				$data['nome'] = $nome;
				$data['titulo'] = $titulo;
				$data['data_inicio'] = $data_inicio;
			}

		}

		$this->load->view('certificados/online', $data);
	}

}

?>