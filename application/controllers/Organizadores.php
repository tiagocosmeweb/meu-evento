<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Organizadores extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');

	}

	public function index()
	{
		$this->load->helper('funcoes');

		$this->load->model('Areaatuacao_model', 'areaatuacao');
		$areas = $this->areaatuacao->selecionar();

		$this->load->model('Nivel_model', 'nivelprofissional');
		$niveis = $this->nivelprofissional->selecionar();

		$this->load->model('Estado_model', 'estado');
		$estados = $this->estado->selecionar();

		$cadastro['areaatuacao'] = $this->funcoes->converter_array_form($areas, 'area');
		$cadastro['nivel'] = $this->funcoes->converter_array_form($niveis, 'nivel');
		$cadastro['estados'] = $this->funcoes->converter_array_form($estados, 'uf');

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu.php');
		$this->load->view('home/home.php');
		$this->load->view('organizadores/cadastro.php', $cadastro);
		$this->load->view('home/rodape.php');

	}

	public function salvar()
	{
		if($this->input->post())
		{

			if($this->input->post('tipo_pessoa') == 'pj')
			{
				$this->form_validation->set_rules('cnpj', 'CNPJ', 'trim|required');
				$this->form_validation->set_rules('razaosocial', 'Razão Social', 'trim|required|min_length[6]');
				$this->form_validation->set_rules('endereco_empresa', 'endereço', 'trim|required');
				$this->form_validation->set_rules('numero_empresa', 'número', 'trim|required');
				$this->form_validation->set_rules('bairro_empresa', 'bairro', 'trim|required');
				$this->form_validation->set_rules('estado_empresa', 'estado', 'trim|required');
				$this->form_validation->set_rules('cidade_empresa', 'cidade', 'trim|required');

			}

			$this->form_validation->set_rules('nomecompleto', 'nome', 'trim|required');
			$this->form_validation->set_rules('datanascimento', 'data de nascimento', 'trim|required|callback_validar_data_nascimento');
			$this->form_validation->set_rules('cpf', 'cpf', 'trim|required|is_unique[organizador.cpf]');
			$this->form_validation->set_rules('areaatuacao', 'área de atuação', 'trim|required');
			$this->form_validation->set_rules('nivel', 'nivel', 'trim|required');
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[organizador.email]');
			$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');

			if(!$this->form_validation->run())
			{
				$this->index();
			}

			else
			{

				$empresa_id = null;
				$data_ativacao = date('Y-m-d h:i:s');


				if($this->input->post('tipo_pessoa') == 'pj')
				{

					$endereco = null;
					$endereco['endereco'] = $this->input->post('endereco_empresa');
					$endereco['numero'] = $this->input->post('numero_empresa');
					$endereco['complemento'] = $this->input->post('complemento_empresa');
					$endereco['bairro'] = $this->input->post('bairro_empresa');
					$endereco['estado_id'] = $this->input->post('estado_empresa');
					$endereco['cidade_id'] = $this->input->post('cidade_empresa');
					$endereco['cep'] = $this->input->post('cep_empresa');

					# inserir endereço da empresa

					$this->load->model('Enderecoempresa_model', 'endereco_empresa');
					$endereco_empresa_id = $this->endereco_empresa->salvar($endereco);

					# inserir empresa

					if(is_int($endereco_empresa_id))
					{
						// cadastrar a empresa
						$this->load->model('Empresa_model', 'empresa');

						# inserir empresa

						$empresa = null;
						$empresa['cnpj'] = $this->input->post('cnpj');
						$empresa['razaosocial'] = $this->input->post('razaosocial');
						$empresa['endereco_id'] = $endereco_empresa_id;
			
						$empresa_id = $this->empresa->salvar($empresa);

					}
				
					# inserir dados pessoais do Organizador

					$organizador = null;
					$organizador['nomecompleto'] = $this->input->post('nomecompleto');
					$organizador['datanascimento'] = $this->funcoes->data_americana($this->input->post('datanascimento'));
					$organizador['cpf'] = $this->input->post('cpf');
					$organizador['area_id'] = $this->input->post('areaatuacao');
					$organizador['nivel_id'] = $this->input->post('nivel');
					$organizador['email'] = $this->input->post('email');
					$organizador['senha'] = $this->encrypt->encode($this->input->post('senha'));
					$organizador['empresa_id'] = $empresa_id;
					$organizador['datacadastro'] = $data_ativacao;
	
					$this->load->model('Organizador_model', 'organizador');
					$id_organizador = $this->organizador->salvar($organizador);
	
					# enviar e-mail de ativação de conta

					$primeiroNome = $this->funcoes->primeiro_nome($organizador['nomecompleto']);
					$email = $organizador['email'];
					$chave_ativacao = $this->funcoes->gerar_token_ativacao();

					$this->load->model('Token_model', 'token');

					$token['organizador_id'] = $id_organizador;
					$token['token'] = $chave_ativacao;
					$token['datacadastro'] = $data_ativacao;
					$token['dataativacao'] = $data_ativacao;

					$this->token->salvar($token);
					$this->emailautomatico->boas_vindas($primeiroNome, $email, $chave_ativacao, $tipo = 'organizador');

					redirect('usuarios/mensagem/cadastro-sucesso');
				}

			}

		}
		else
			redirect('/participantes');
	}

	function validar_data_nascimento($string)
	{

		$data = explode('/', $string);

		if(checkdate ($data[1] , $data[0] , $data[2]))
		{
			return true;	
		}

		else
		{
			$this->form_validation->set_message('validar_data_nascimento', 'Digite uma data de nascimento válida');
			return FALSE;
		}

	}

	public function painel()
	{		
		$this->sessao->consultar('id');

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('organizadores/painel.php');
		$this->load->view('home/rodape.php');
	}

}

?>