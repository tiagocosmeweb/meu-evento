<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
		$this->load->library('uploadarquivo');
		$this->load->model('Evento_model', 'evento');

	}

	public function novo($evento_id)
	{
		$this->sessao->consultar('id');

		$data['evento_id'] = $evento_id;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('faqs/novo.php', $data);
		$this->load->view('home/rodape.php');
	}


	public function editar($questao_id, $evento_id)
	{
		$this->sessao->consultar('id');

		$data['evento_id'] = $evento_id;

		$this->load->model('Faq_model', 'faq');
		$data['questao'] = $this->faq->get((int) $questao_id);

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('faqs/editar.php', $data);
		$this->load->view('home/rodape.php');
	}


	public function gerenciar($evento_id)
	{
		$this->sessao->consultar('id');

		$condicoes['evento_id'] = $evento_id;
		$this->load->model('Faq_model', 'faq');
		$questoes = $this->faq->condicoes($condicoes, $campos = 'id, pergunta, situacao');

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');

		$data['questoes'] =  $questoes;
		$data['evento_id'] = $evento_id;
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('faqs/gerenciar.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function salvar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			$evento_id = $this->input->post('evento_id');

			$this->form_validation->set_rules('pergunta', 'pergunta', 'trim|required');
			$this->form_validation->set_rules('resposta', 'resposta', 'trim|required');
			$this->form_validation->set_rules('evento_id', 'evento', 'trim|required');
	
			if(!$this->form_validation->run())
			{
				$this->novo($evento_id);
			}

			else
			{

				$faq['pergunta']  = $this->input->post('pergunta');
				$faq['resposta']  = $this->input->post('resposta');
				$faq['evento_id'] = $this->input->post('evento_id');

				$this->load->model('Faq_model', 'faq');

				if(is_int($this->faq->salvar($faq)))
					{
						$this->session->set_flashdata('mensagem', 'Questão inserida com sucesso');
						redirect(base_url('faqs/gerenciar/' . $this->input->post('evento_id')));
					}
					else
					{
						exit('não foi possível cadastrar');
					}

			}

		}
		else
			redirect(base_url('/organizadores'));
	}


	public function atualizar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			$evento_id = $this->input->post('evento_id');
			$id = $this->input->post('id');

			$this->form_validation->set_rules('pergunta', 'pergunta', 'trim|required');
			$this->form_validation->set_rules('resposta', 'resposta', 'trim|required');
			$this->form_validation->set_rules('evento_id', 'evento', 'trim|required');
			$this->form_validation->set_rules('id', 'id', 'trim|required');
	
			if(!$this->form_validation->run())
			{
				$this->editar($this->input->post('id'), $this->input->post('evento_id'));
			}

			else
			{
				$faq = null;
				$faq['pergunta']  = $this->input->post('pergunta');
				$faq['resposta']  = $this->input->post('resposta');
				$faq['evento_id'] = $this->input->post('evento_id');
				$faq['situacao'] = $this->input->post('situacao');

				$this->load->model('Faq_model', 'faq');

				$this->faq->salvar($faq, $acao = 'alterar', $this->input->post('id'));
				$this->session->set_flashdata('mensagem', 'Questão alterada com sucesso');
				redirect(base_url('faqs/gerenciar/' . $this->input->post('evento_id')));
			}

		}
		else
			redirect(base_url('/organizadores'));
	}

	public function deletar($id)
	{

		$evento = $this->input->get('evento');
		$this->load->model('Faq_model', 'faq');

		if($this->faq->apagar($id) == 1)
		{
			$this->session->set_flashdata('mensagem', 'Questão deletada com sucesso.');
			redirect(base_url('/faqs/gerenciar/' . $evento));
		}

		else
		{
			exit('Questão não encontrada');
		}

	}
}

?>