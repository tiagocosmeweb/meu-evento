<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
		$this->load->library('uploadarquivo');

		$this->load->model('Video_model', 'video');
		$this->load->model('Evento_model', 'evento');

	}

	public function novo($evento_id)
	{
		$this->sessao->consultar('id');
		
		$data['evento_id'] = $evento_id;

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('videos/novo.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function gerenciar($evento_id)
	{
		$this->sessao->consultar('id');

		$condicoes['evento_id'] = $evento_id;
		$videos = $this->video->condicoes($condicoes, $campos = 'id, titulo');

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');

		$data['videos'] = $videos;
		$data['evento_id'] = $evento_id;
		$data['evento_selecionado'] = $evento_selecionado;

		// echo "<pre>" . print_r($videos, true) . "</pre>";

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('videos/gerenciar.php', $data);
		$this->load->view('home/rodape.php');
	}


	function validar_arquivo($string)
	{

		$evento_id = $this->input->post('evento_id');
		$retorno_upload = $this->uploadarquivo->upload_arquivo($evento_id, $formato = 'todos');

		if(!$retorno_upload)
		{
			$this->form_validation->set_message('validar_arquivo', 'Arquivo não permitido');
			return false;
		}
		else
		{
			return true;
		}

	}

	public function salvar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			$evento_id = $this->input->post('evento_id');

			$this->form_validation->set_rules('titulo', 'nome', 'trim|required');
			$this->form_validation->set_rules('descricao', 'descricao', 'trim|required');
			$this->form_validation->set_rules('video_incorporado', 'descricao', 'trim|required');

			if(!$this->form_validation->run())
			{
				$this->novo($evento_id);
			}

			else
			{

				$video = null;
				$video['titulo'] = $this->input->post('titulo');
				$video['descricao'] = $this->input->post('descricao');
				$video['video_incorporado'] = $this->input->post('video_incorporado');
				$video['evento_id'] = $this->input->post('evento_id');
			
				if(is_int($this->video->salvar($video)))
				{
					$this->session->set_flashdata('mensagem', 'Vídeo cadastrado com sucesso.');
					redirect(base_url('/videos/gerenciar/' . $this->input->post('evento_id')));
				}
				else
				{
					exit('não foi possível cadastrar o vídeo.');
				}

			}

		}
		else
			redirect(base_url('/participantes'));
	}

	public function deletar($id)
	{

		$evento = $this->input->get('evento');

		if($this->video->apagar($id) == 1)
		{
			$this->session->set_flashdata('mensagem', 'Vídeo deletado com sucesso.');
			redirect(base_url('/videos/gerenciar/' . $evento));

		}
		else
		{
			exit('Vídeo não encontrado');
		}

	}
}

?>