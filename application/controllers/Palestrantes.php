<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Palestrantes extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
		$this->load->library('uploadarquivo');

		$this->load->model('Evento_model', 'evento');

	}

	public function novo($evento_id)
	{
		$this->sessao->consultar('id');

		$data['evento_id'] = $evento_id;

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('palestrantes/novo.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function gerenciar($evento_id)
	{
		$this->sessao->consultar('id');

		$condicoes['evento_id'] = $evento_id;
		$this->load->model('Palestrante_model', 'palestrante');
		$palestrantes = $this->palestrante->condicoes($condicoes, $campos = 'id, nome');

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');

		$data['palestrantes'] = $palestrantes;
		$data['evento_id'] = $evento_id;
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php'); 
		$this->load->view('home/home.php');
		$this->load->view('palestrantes/gerenciar.php', $data);
		$this->load->view('home/rodape.php');
	}


	function validar_arquivo($string)
	{

		$evento_id = $this->input->post('evento_id');
		$retorno_upload = $this->uploadarquivo->upload_arquivo($evento_id);

		if(!$retorno_upload)
		{
			$this->form_validation->set_message('validar_arquivo', 'Arquivo não permitido');
			return false;
		}
		else
		{
			return true;
		}

	}

	public function salvar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			$evento_id = $this->input->post('evento_id');

			$this->form_validation->set_rules('nome', 'nome', 'trim|required');
			$this->form_validation->set_rules('email', 'e-mail', 'trim|required|valid_email');
			$this->form_validation->set_rules('resumo_profissional', 'resumo profissional', 'trim|required');
			$this->form_validation->set_rules('arquivo', 'imagem', 'callback_validar_arquivo');

			if(!$this->form_validation->run())
			{
				$this->novo($evento_id);
			}

			else
			{

				$palestrante['nome'] = $this->input->post('nome');
				$palestrante['email'] = $this->input->post('email');
				$palestrante['resumo_profissional'] = $this->input->post('resumo_profissional');
				$palestrante['foto'] = $_POST['imagem']['sucesso']['file_name'];
				$palestrante['evento_id'] = $this->input->post('evento_id');

				$this->load->model('Palestrante_model', 'palestrante');
				
				if(is_int($this->palestrante->salvar($palestrante)))
				{
					$this->session->set_flashdata('mensagem', 'Palestrante cadastrado com sucesso.');
					redirect(base_url('/palestrantes/gerenciar/' . $this->input->post('evento_id')));
				}
				else
				{
					exit('não foi possível cadastrar o seu evento');
				}

			}

		}
		else
			redirect(base_url('/participantes'));
	}

	public function deletar($id)
	{

		$evento = $this->input->get('evento');
		$this->load->model('Palestrante_model', 'palestrante');

		$palestrante = $this->palestrante->get((int) $id);

		if($this->palestrante->apagar($id) == 1)
		{
			$nome_arquivo = $palestrante[0]->foto;

			$diretorio = '/public_html/meuevento.de/projeto/uploads/';
			$arquivo = $diretorio .  $evento .'/'. $nome_arquivo;

			$conn_id = ftp_connect('192.185.176.243');
			$login_result = ftp_login($conn_id, 'tiago275', '41lhYUx8m6');
			ftp_delete($conn_id, $arquivo);

			$this->session->set_flashdata('mensagem', 'Palestrante deletado com sucesso.');
			redirect(base_url('/palestrantes/gerenciar/' . $evento));

		}
		else
		{
			exit('Palestrante não encontrado');
		}

	}
}

?>