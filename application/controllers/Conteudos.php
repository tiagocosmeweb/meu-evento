<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Conteudos extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
		$this->load->library('uploadarquivo');

		$this->load->model('Conteudo_model', 'conteudo');
		$this->load->model('Evento_model', 'evento');

	}

	public function novo($evento_id)
	{
		$data['evento_id'] = $evento_id;

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('conteudos/novo.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function gerenciar($evento_id)
	{
		$this->sessao->consultar('id');

		$condicoes['evento_id'] = $evento_id;
		$conteudos = $this->conteudo->condicoes($condicoes, $campos = 'id, titulo');

		$evento_selecionado = $this->evento->get((int) $evento_id, $campos = 'titulo');

		$data['conteudos'] = $conteudos;
		$data['evento_id'] = $evento_id;
		$data['evento_selecionado'] = $evento_selecionado;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu_evento_login.php');
		$this->load->view('home/home.php');
		$this->load->view('conteudos/gerenciar.php', $data);
		$this->load->view('home/rodape.php');
	}


	function validar_arquivo($string)
	{

		$evento_id = $this->input->post('evento_id');
		$retorno_upload = $this->uploadarquivo->upload_arquivo($evento_id, $formato = 'todos');

		if(!$retorno_upload)
		{
			$this->form_validation->set_message('validar_arquivo', 'Arquivo não permitido');
			return false;
		}
		else
		{
			return true;
		}

	}

	public function salvar()
	{
		$this->sessao->consultar('id');

		if($this->input->post())
		{
			$evento_id = $this->input->post('evento_id');

			$this->form_validation->set_rules('titulo', 'nome', 'trim|required');
			$this->form_validation->set_rules('arquivo', 'imagem', 'callback_validar_arquivo');

			if(!$this->form_validation->run())
			{
				$this->novo($evento_id);
			}

			else
			{

				$conteudo = null;
				$conteudo['titulo'] = $this->input->post('titulo');
				$conteudo['arquivo'] = $_POST['imagem']['sucesso']['file_name'];
				$conteudo['evento_id'] = $this->input->post('evento_id');
			
				if(is_int($this->conteudo->salvar($conteudo)))
				{
					$this->session->set_flashdata('mensagem', 'Conteúdo cadastrado com sucesso.');
					redirect(base_url('/conteudos/gerenciar/' . $this->input->post('evento_id')));
				}
				else
				{
					exit('não foi possível cadastrar o seu conteúdo');
				}

			}

		}
		else
			redirect(base_url('/participantes'));
	}

	public function deletar($id)
	{

		$evento = $this->input->get('evento');

		$conteudo = $this->conteudo->get((int) $id);

		if($this->conteudo->apagar($id) == 1)
		{
			$nome_arquivo = $conteudo[0]->arquivo;

			$diretorio = '/public_html/meuevento.de/projeto/uploads/';
			$arquivo = $diretorio .  $evento .'/'. $nome_arquivo;

			$conn_id = ftp_connect('192.185.176.243');
			$login_result = ftp_login($conn_id, 'tiago275', '41lhYUx8m6');
			ftp_delete($conn_id, $arquivo);

			$this->session->set_flashdata('mensagem', 'Conteúdo deletado com sucesso.');
			redirect(base_url('/conteudos/gerenciar/' . $evento));

		}
		else
		{
			exit('Conteúdo não encontrado');
		}

	}
}

?>