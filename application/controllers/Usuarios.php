<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('encrypt');
		$this->load->library('funcoes');
		$this->load->library('emailautomatico');
		$this->load->library('sessao');
	}

	public function mensagem($msg = '')
	{

		$data['mensagem'] = $msg;

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu.php');
		$this->load->view('home/home.php');
		$this->load->view('home/mensagem.php', $data);
		$this->load->view('home/rodape.php');
	}

	public function ativador($email, $chave_ativacao, $tipo = 'participante')
	{

	    $this->load->model('Token_model', 'token');

	    if($tipo == 'participante')
	    {

		   	$validacao = $this->token->ativador_participante($email, $chave_ativacao);

		    if(count($validacao) == 1)
		    {

		    	if($validacao[0]->situacao == 0)
		    	{

		    		$this->load->model('Participante_model', 'participante');
		    		$participante['situacao'] = 1;
					$this->participante->salvar($participante, $tipo = 'editar', $validacao[0]->idparticipante);

					$this->token->apagar($validacao[0]->idtoken);
					redirect(base_url('usuarios/mensagem/cadastro-confirmacao'));

		    	}	
		    }
		    
		    else
		    {
				redirect(base_url('usuarios/mensagem/cadastro-ja-confirmado'));
		    }
	    }

	    if($tipo == 'organizador')
	    {

		   	$validacao = $this->token->ativador_organizador($email, $chave_ativacao);

		    if(count($validacao) == 1)
		    {

		    	if($validacao[0]->situacao == 0)
		    	{

		    		$this->load->model('Organizador_model', 'organizador');
		    		$organizador['situacao'] = 1;
					$this->organizador->salvar($organizador, $tipo = 'editar', $validacao[0]->idorganizador);

					$this->token->apagar($validacao[0]->idtoken);
					redirect('usuarios/mensagem/cadastro-confirmacao');
		    	}	
		    }
		    
		    else
		    {
				redirect(base_url('usuarios/mensagem/cadastro-ja-confirmado'));
		    }
	    }

	}

	public function esqueci_senha()
	{
		$encerrar = false;

		if(@$this->input->post())
		{
			$encerrar = true;

			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
			$this->form_validation->set_rules('tipocadastro', 'tipo de cadastro', 'trim|required');

			if(!$this->form_validation->run())
			{
				$this->esqueci_senha();
			}

			else
			{
				$dados['email'] = $this->input->post('email');
				
				# armazena o tipo de cadastro

				$tipocadastro = $this->input->post('tipocadastro');

				# tipo participante

				if($tipocadastro == 'participante')
				{
					$tipocadastro = 'participante';
					
					$this->load->model('Participante_model', 'participante');
					$participante = $this->participante->get($dados);

					if(count($participante) == 1)
					{

						$id = $participante[0]->id;
						$primeiroNome = $this->funcoes->primeiro_nome($participante[0]->nomecompleto);
						$email = $participante[0]->email;
						$chave_ativacao = $this->funcoes->gerar_token_ativacao();

						$this->load->model('Token_model', 'token');

						$data_ativacao = date('Y-m-d h:i:s');

						$token['participante_id'] = $id;
						$token['token'] = $chave_ativacao;
						$token['datacadastro'] = $data_ativacao;
						$token['dataativacao'] = $data_ativacao;

						$this->token->salvar($token);
						$this->emailautomatico->alterar_senha($primeiroNome, $email, $chave_ativacao, $tipocadastro);
						redirect('usuarios/mensagem/email-alterar-senha');
					}

					else
					{
						redirect('usuarios/mensagem/email-nao-encontrado');
					}

				}

				else if($tipocadastro == 'organizador')
				{
					$tipocadastro = 'organizador';
					
					$this->load->model('Organizador_model', 'organizador');
					$organizador = $this->organizador->get($dados);

					if(count($organizador) == 1)
					{

						$id = $organizador[0]->id;
						$primeiroNome = $this->funcoes->primeiro_nome($organizador[0]->nomecompleto);
						$email = $organizador[0]->email;
						$chave_ativacao = $this->funcoes->gerar_token_ativacao();

						$this->load->model('Token_model', 'token');

						$data_ativacao = date('Y-m-d h:i:s');

						$token['participante_id'] = $id;
						$token['token'] = $chave_ativacao;
						$token['datacadastro'] = $data_ativacao;
						$token['dataativacao'] = $data_ativacao;

						$this->token->salvar($token);
						$this->emailautomatico->alterar_senha($primeiroNome, $email, $chave_ativacao, $tipocadastro);
						redirect('usuarios/mensagem/email-alterar-senha');
					}

					else
					{
						redirect('usuarios/mensagem/email-nao-encontrado');
					}
				}
				
			}

		}
		if($encerrar == false)
		{
			$this->load->view('home/cabecalho.php');
			$this->load->view('home/menu.php');
			$this->load->view('home/home.php');
			$this->load->view('participantes/esqueci_senha.php');
			$this->load->view('home/rodape.php');
		}

	}

	public function nova_senha($email, $chave_ativacao, $tipo_cadastro)
	{

	   $this->load->model('Token_model', 'token');

	    if($tipo_cadastro == 'participante')
	   		$validacao = $this->token->ativador_participante($email, $chave_ativacao);

	   	if($tipo_cadastro == 'organizador')
	   	    $validacao = $this->token->ativador_organizador($email, $chave_ativacao);


	    if(count($validacao) == 1)
	    {   
	    	$data['email'] = $email;
	    	$data['chave_ativacao'] = $chave_ativacao;
	    	$data['tipo_cadastro'] = $tipo_cadastro;

	    	$this->load->view('home/cabecalho.php');
		    $this->load->view('home/menu.php');
		    $this->load->view('home/home.php');
		    $this->load->view('participantes/nova_senha.php', $data);
		    $this->load->view('home/rodape.php');
	     
	    }
	    
	    else
	    {
			redirect('usuarios/mensagem/email-chave-invalida');
	    }

	}

	public function salvar_senha()
	{

		if($this->input->post())
		{
			$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('email', 'email', '');
			$this->form_validation->set_rules('chave_ativacao', 'chave de ativação');
			$this->form_validation->set_rules('tipo_cadastro', 'tipo de cadastro');

			if(!$this->form_validation->run())
			{
				$this->nova_senha();
			}

			else
			{
				$email = $this->input->post('email');
				$chave_ativacao = $this->input->post('chave_ativacao');
				$nova_senha = $this->encrypt->encode($this->input->post('senha'));

				$tipo_cadastro = $this->input->post('tipo_cadastro');
				$this->load->model('Token_model', 'token');

				if($tipo_cadastro == 'participante')
				{
	   				$validacao = $this->token->ativador_participante($email, $chave_ativacao);

		   			if(count($validacao) == 1)
		   			{
						$participante['senha'] = $nova_senha;

						$this->load->model('Participante_model', 'participante');
						$this->participante->salvar($participante, $tipo = 'editar', $validacao[0]->idparticipante);

						$this->token->apagar($validacao[0]->idtoken);
						redirect('usuarios/mensagem/senha-alterada-sucesso');
		   			}
				}

	   			if($tipo_cadastro == 'organizador')
	   			{
	   	    		$validacao = $this->token->ativador_organizador($email, $chave_ativacao);

		   			if(count($validacao) == 1)
		   			{
						$organizador['senha'] = $nova_senha;

						$this->load->model('Organizador_model', 'organizador');
						$this->organizador->salvar($organizador, $tipo = 'editar', $validacao[0]->idorganizador);

						$this->token->apagar($validacao[0]->idtoken);
						redirect('usuarios/mensagem/senha-alterada-sucesso');
		   			}
	   			}

	   			
			}
		}

		else
		{
			redirect('/');
		}

	}

	public function login($msg = null)
	{

		$data['mensagem'] = $msg;

		// parâmetro get para orientar o login
		$data['cadastro'] = $this->input->get('cadastro');

		$this->load->view('home/cabecalho.php');
		$this->load->view('home/menu.php');
		$this->load->view('home/home.php');
		$this->load->view('usuarios/login.php', $data);
		$this->load->view('home/rodape.php');

	}

	public function logar()
	{

			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
			$this->form_validation->set_rules('senha', 'senha', 'trim|required|min_length[6]');
			$this->form_validation->set_rules('tipocadastro', 'tipo de cadastro', 'trim|required');

			if(!$this->form_validation->run())
			{ 	
				$this->login();
			}

			else
			{ 
				// $this->sessao->apagar();

				$tipocadastro = $this->input->post('tipocadastro');
				$dados['email'] = $this->input->post('email');

				if($tipocadastro == 'participante') 
				{

					$this->load->model('Participante_model', 'participante');
					$login = $this->participante->condicoes($dados, $campos = '*');

					// var_dump($login);
					// exit;

				    if(count($login) == 1)
					{
						if($login[0]->situacao == 0)
						    $this->login($msg = 'cadastro-nao-confirmado');
						
						else
						{
							$senha_form  = $this->input->post('senha');
							$senha_banco = $this->encrypt->decode($login[0]->senha);

							if($senha_form == $senha_banco)
							{
								// echo "<pre>" . print_r($login, true) . "</pre>";

								$palestrante = null;
								$palestrante['id'] = $login[0]->id;
								$palestrante['nome'] = $this->funcoes->primeiro_nome($login[0]->nomecompleto);
								$palestrante['tipo'] = 'participante';

								$this->sessao->criar($palestrante);
								redirect(base_url('participantes/painel'));
							}

							else
								$this->login($msg = 'senha-invalida');
						}
					}

					else
						$this->login($msg = 'credencial-nao-encontrada');



		    		
		    		
				}

				if($tipocadastro == 'organizador')
				{

					$this->load->model('Organizador_model', 'organizador');
		    		$login = $this->organizador->condicoes($dados, $campos = '*');

				    if(count($login) == 1)
					{
						if($login[0]->situacao == 0)
						    $this->login($msg = 'cadastro-nao-confirmado');
						
						else
						{
							$senha_form  = $this->input->post('senha');
							$senha_banco = $this->encrypt->decode($login[0]->senha);

							if($senha_form == $senha_banco)
							{
								// echo "<pre>" . print_r($login, true) . "</pre>";

								$organizador = null;
								$organizador['id'] = $login[0]->id;
								$organizador['empresa_id'] = $login[0]->empresa_id;
								$organizador['nome'] = $this->funcoes->primeiro_nome($login[0]->nomecompleto);
								$organizador['tipo'] = 'organizador';

								$this->sessao->criar($organizador);
								redirect(base_url('organizadores/painel'));
							}

							else
								$this->login($msg = 'senha-invalida');
						}
					}

					else
						$this->login($msg = 'credencial-nao-encontrada');

				}

			}
	}

	public function sair()
	{
		$this->sessao->apagar();
		redirect(base_url('usuarios/login'));
	}
}

?>