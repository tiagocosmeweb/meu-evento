<?php

class Sessao
{
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->library('session');
	}

	public function criar($data)
	{

		$this->CI->session->set_userdata($data);
	}

	public function apagar()
	{
		$this->CI->session->sess_destroy();
	}

	public function consultar($chave)
	{

		if(!$this->CI->session->has_userdata($chave))
		{
			$this->apagar();
			redirect('usuarios/login/permissao-negada');
		}

		else
		{
			return true;
		}
	}


	public function get($atributo = false)
	{
		if($atributo == false)
			return $this->CI->session->userdata();
		else
			return $this->CI->session->userdata($atributo);
	}

}

?>