<?php

class UploadArquivo
{

	public function __construct()
	{
		$this->CI = &get_instance();
	}
	
    public function upload_arquivo($evento_id, $formato = 'imagem')
    {
        // pasta não existe. É automaticamente criada.
        if((!file_exists ('./uploads/' . $evento_id)))
        {
            mkdir('./uploads/' . $evento_id, 0777); 
        }

        $config['upload_path'] = './uploads/' . $evento_id;

        if($formato == 'imagem')
        {  
            $config['allowed_types']  = 'gif|jpg|png|jpeg|png';
        }

        else
        {
            $config['allowed_types']  = 'gif|jpg|png|pdf|doc|docx|xsl|xsls|zip|jpeg|png';
        }

        $this->CI->load->library('upload', $config);

        if (!$this->CI->upload->do_upload('arquivo'))
        {

            //$erro = $this->CI->upload->display_errors();
            return  false; 

        }
        else
        {
            $sucesso = array('sucesso' =>$this->CI->upload->data());
            $_POST['imagem'] = $sucesso;
            return true; 
        }
    }
}
?>