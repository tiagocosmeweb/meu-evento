<?php

class EmailAutomatico
{
	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->library('email');
	}

	public function boas_vindas($primeiroNome, $email, $chave_ativacao, $tipo = 'participante')
	{

	    $this->CI->email->from('automatico@meuevento.de', 'Meu Evento');
	    $this->CI->email->to($email);
	    $this->CI->email->bcc('automatico@meuevento.de');
	    $this->CI->email->subject($primeiroNome . ' ative o seu cadastro!');

	    $dados['chave_ativacao'] = $chave_ativacao;
	    $dados['primeiroNome'] = $primeiroNome;
	    $dados['email'] = $email;
	    $dados['tipo'] = $tipo;

	    $html = $this->CI->load->view('emails/boas_vindas', $dados, true);
	    $this->CI->email->message($html);
	    $this->CI->email->send();
	}


	public function alterar_senha($primeiroNome, $email, $chave_ativacao, $tipocadastro)
	{

	    $this->CI->email->from('automatico@meuevento.de', 'Meu Evento');
	    $this->CI->email->to($email);
	    $this->CI->email->bcc('automatico@meuevento.de');
	    $this->CI->email->subject($primeiroNome . ', recupere sua senha');

	    $dados['chave_ativacao'] = $chave_ativacao;
	    $dados['primeiroNome'] = $primeiroNome;
	    $dados['email'] = $email;
	    $dados['tipocadastro'] = $tipocadastro;

	    $html = $this->CI->load->view('emails/alterar_senha', $dados, true);
	    $this->CI->email->message($html);
	    $this->CI->email->send();
	}


	public function enviar_email($assunto, $mensagem, $emails)
	{

	    $this->CI->email->from('automatico@meuevento.de', 'Meu Evento');
	    $this->CI->email->to($emails);
	    $this->CI->email->bcc('automatico@meuevento.de');
	    $this->CI->email->subject($assunto);
	    $this->CI->email->message($mensagem);
	    $this->CI->email->send();
	}


	public function certificado($primeiroNome, $email, $link_certificado)
	{

		$dados['primeiroNome'] = $primeiroNome;
		$dados['link_certificado'] = $link_certificado;

	    $this->CI->email->from('automatico@meuevento.de', 'Meu Evento');
	    $this->CI->email->to($email);
	    $this->CI->email->bcc('automatico@meuevento.de');
	    $this->CI->email->subject($primeiroNome . ', seu certificado de participação');
	    $html = $this->CI->load->view('emails/certificado', $dados, true);
	    $this->CI->email->message($html);
	    $this->CI->email->send();

	}
}

?>