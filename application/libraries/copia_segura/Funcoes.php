<?php

class Funcoes
{
	public function data_americana($string)
	{
		$data = explode('/', $string);

		if(checkdate ($data[1] , $data[0] , $data[2]))
		{
			return $data[2] .'/'. $data[1] .'/'. $data[0];
		}

		else
			return null;
	}


	public function data_brasil($string, $funcionalidade = 'data')
	{

		if($funcionalidade == 'data')
		{
			return date('d/m/Y', strtotime($string));
		}

		if($funcionalidade == 'hora')
		{
			return date('h:i', strtotime($string));
		}

		if($funcionalidade == 'data_hora')
		{
			return date('d/m/Y h:i:s', strtotime($string));
		}

	}

	public function formata_data_evento($data_evento)
	{

		$data = explode(' ', $data_evento);
		$hora = $data[1];
		$data = explode('/', $data[0]);
		$data = $data[2] . '-' . $data[1]  . '-' . $data[0];

		return $data . ' ' . $hora ;
	}


	function converter_array_form($vetor, $campo = 'area')
	{
		$monta_vetor = null;
		$monta_vetor = array();

		$monta_vetor[''] = 'Selecione';

		foreach($vetor as $valor)
		{		
		   $monta_vetor[$valor['id']] = $valor[$campo];
		}

		return $monta_vetor;
	}

	function primeiro_nome($string)
	{
		$nome = explode(' ', $string);
		return ucfirst($nome[0]);
	}

	function gerar_token_ativacao()
	{
		return rand(5, 1000) . date('Ymdhis'); 
	}

}

?>