<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Certificados extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('funcoes');
		$this->load->library('sessao');
		$this->load->library('emailautomatico');

	}

	public function gerar($evento_id)
	{
		// participantes ativos

		$condicoes = null;
		$condicoes['evento_id'] = $evento_id;
		$condicoes['presenca'] = 1;
		$condicoes['situacao'] = 1;

		$this->load->model('Inscricao_model', 'inscricao');
		$inscricoes = $this->inscricao->condicoes($condicoes);

		if(count($inscricoes) >= 1)
		{

			$this->load->model('participante_model', 'participante');
			$this->load->model('Certificado_model', 'certificado');

			foreach($inscricoes as $inscricao)
			{

				$participante = $this->participante->get((int) $inscricao->participante_id, $campos = 'id, nomecompleto, email');
				
				// gerar chave de acesso ao certificado

				$certificado = null;
				$certificado['chave'] = date('dmyhis') . rand(0, 1000); 
				$certificado['tipo'] = 'participante'; 
				$certificado['evento_id'] = $evento_id; 
				$certificado['participante_id'] = $participante[0]->id; 
				$this->certificado->salvar($certificado);

				$primeiroNome = $this->funcoes->primeiro_nome($participante[0]->nomecompleto);
				$email = $participante[0]->email;
				$link_certificado = $certificado['chave'];

				$this->emailautomatico->certificado($primeiroNome, $email, $link_certificado);

			}

		}
		
		// palestrantes

		$condicoes = null;
		$condicoes['evento_id'] = $evento_id;

		$this->load->model('Palestrante_model', 'palestrante');
		$palestrantes = $this->palestrante->condicoes($condicoes, $campos = 'id, nome, email');

		if(count($palestrantes) >= 1)
		{
			foreach($palestrantes as $palestrante)
			{
				// gerar chave de acesso ao certificado

				$certificado = null;
				$certificado['chave'] = date('dmyhis') . rand(0, 1000); 
				$certificado['tipo'] = 'palestrante'; 
				$certificado['evento_id'] = $evento_id; 
				$certificado['palestrante_id'] = $palestrante->id; 
				$this->certificado->salvar($certificado);

				$primeiroNome = $this->funcoes->primeiro_nome($palestrante->nome);
				$email = $palestrante->email;
				$link_certificado = $certificado['chave'];

				$this->emailautomatico->certificado($primeiroNome, $email, $link_certificado);
			}
		}

		$this->session->set_flashdata('mensagem', 'Certificado enviado com sucesso.');
		redirect(base_url('/eventos/gerenciar'));
	}

	function online($chave)
	{
		$this->load->model('Certificado_model', 'certificado');

		$data['chave'] = $chave;
		$verificar_chave = $this->certificado->condicoes($data);

		echo "<pre>" . print_r($verificar_chave, true) . "</pre>";
	}

}

?>