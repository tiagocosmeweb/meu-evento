<?php

class Funcoes
{
	public function data_americana($string, $separador = '/')
	{
		$data = explode('/', $string);

		if(checkdate ($data[1] , $data[0] , $data[2]))
		{
			return $data[2] . $separador . $data[1] . $separador . $data[0];
		}

		else
			return null;
	}

	public function data_brasil($string, $funcionalidade = 'data')
	{

		if($funcionalidade == 'data')
		{
			return date('d/m/Y', strtotime($string));
		}

		if($funcionalidade == 'hora')
		{
			return date('h:i', strtotime($string));
		}

		if($funcionalidade == 'data_hora')
		{
			return date('d/m/Y h:i:s', strtotime($string));
		}

	}

	function converter_array_form($vetor, $campo = 'area', $selecione = true)
	{
		$monta_vetor = null;
		$monta_vetor = array();

		if($selecione)
			$monta_vetor[''] = 'Selecione';

		foreach($vetor as $valor)
		{	
		   $monta_vetor[$valor['id']] = $valor[$campo];
		}

		return $monta_vetor;
	}

	function primeiro_nome($string)
	{
		$nome = explode(' ', $string);
		return ucfirst($nome[0]);
	}

	function gerar_token_ativacao()
	{
		return rand(5, 1000) . date('Ymdhis'); 
	}

	function valor_decimal($valor)
	{
		$valor = str_replace('.','', $valor);
		$valor = str_replace(',','.', $valor);
		return $valor;
	}

}

?>