<?php

class UploadArquivo
{

	public function __construct()
	{
		$this->CI = &get_instance();
	}
	
    public function upload_arquivo($solicitacao_id)
    {
        // pasta não existe. É automaticamente criada.
        if((!file_exists ('./uploads/' . $solicitacao_id)))
        {
            mkdir('./uploads/' . $solicitacao_id, 0777); 
        }

        $config['upload_path']          = './uploads/' . $solicitacao_id;
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx|xsl|xsls|zip|jpeg|png';

        $this->CI->load->library('upload', $config);

        if (!$this->CI->upload->do_upload('arquivo'))
        {
            // print_r($this->CI->upload->display_errors());
           return false;
        }
        else
        {
            $sucesso = array('sucesso' =>$this->CI->upload->data());
            return $sucesso; 
        }
    }
}
?>