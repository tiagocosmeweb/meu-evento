<?php

class Paginacao
{

	public function __construct()
	{
		$this->CI = &get_instance();
		$this->CI->load->library('pagination');
	}

	public function paginar($url, $total, $por_pagina)
	{

		$config['base_url'] = base_url($url);
		$config['total_rows'] = $total;
		$config['per_page'] =  $por_pagina;
		$config['first_link'] = 'Primeira';
		$config['last_link'] = 'Última';
		$config['prev_link'] = 'Voltar';
		$config['next_link'] = 'Próximo';
		//$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['reuse_query_string'] = TRUE;

		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		# solução para resolver o css da página atual.
		$config['cur_tag_open'] = '<li class="active"> <a href="http://localhost/aguiaresilva/advogados/gerenciar/#">';
		$config['cur_tag_close'] =  '</a></li>';

		$this->CI->pagination->initialize($config);
		return $this->CI->pagination->create_links();

	}
}

?>