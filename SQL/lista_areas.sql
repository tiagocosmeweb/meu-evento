-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2016 at 10:49 AM
-- Server version: 5.5.38-35.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tiago275_talentos`
--

-- --------------------------------------------------------

--
-- Table structure for table `lista_areas`
--

CREATE TABLE IF NOT EXISTS `lista_areas` (
  `id` int(11) NOT NULL,
  `area` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lista_areas`
--

INSERT INTO `lista_areas` (`id`, `area`) VALUES
(1, 'Administração Comercial/Vendas'),
(2, '\r\nAdministração de Empresas'),
(3, '\r\nAdministração Pública'),
(4, '\r\nAdvocacia/Jurídica'),
(5, '\r\nAgronomia'),
(6, '\r\nArquitetura/Urbanismo'),
(7, '\r\nArquivologia'),
(8, '\r\nArtes Cênicas'),
(9, '\r\nArtes Gráficas'),
(10, '\r\nArtes Plásticas'),
(11, '\r\nAtend. a cliente - Bares e Restaurantes'),
(12, '\r\nAtend. a cliente - Telefonistas/Recepcionistas'),
(13, '\r\nAtend. a cliente - Telemarketing/Call Center'),
(14, '\r\nAtend. a cliente'),
(15, '\r\nAuditoria'),
(16, '\r\nBancos'),
(17, '\r\nBiblioteconomia'),
(18, '\r\nBiologia/Ciências Biológicas'),
(19, '\r\nBiomedicina'),
(20, '\r\nCiências Sociais'),
(21, '\r\nCinema'),
(22, '\r\nComércio Exterior'),
(23, '\r\nCompras'),
(24, '\r\nComunicação'),
(25, '\r\nConstrução Civil'),
(26, '\r\nContabilidade'),
(27, '\r\nControladoria'),
(28, '\r\nCulinária/Gastronomia'),
(29, '\r\nDesenho Industrial'),
(30, '\r\nDesign de Interiores'),
(31, '\r\nDesign Gráfico'),
(32, '\r\nEcologia/Meio Ambiente'),
(33, '\r\nEconomia'),
(34, '\r\nEnfermagem'),
(35, '\r\nEngenharia de Alimentos'),
(36, '\r\nEngenharia Civil'),
(37, '\r\nEngenharia Eletrônica'),
(38, '\r\nEngenharia Eletrotécnica'),
(39, '\r\nEngenharia Mecânica'),
(40, '\r\nEngenharia Metalúrgica e de Materiais'),
(41, '\r\nEngenharia de Minas'),
(42, '\r\nEngenharia de Produção'),
(43, '\r\nEngenharia Química'),
(44, '\r\nEngenharia - Outras'),
(45, '\r\nEnsino Superior e Pesquisa'),
(46, '\r\nEnsino - Outros'),
(47, '\r\nEntretenimento'),
(48, '\r\nEsportes'),
(49, '\r\nEstética'),
(50, '\r\nFarmácia'),
(51, '\r\nFilosofia'),
(52, '\r\nFinanças'),
(53, '\r\nFiscal'),
(54, '\r\nFísica'),
(55, '\r\nFisioterapia'),
(56, '\r\nFonoaudiologia'),
(57, '\r\nGeografia'),
(58, '\r\nGeologia'),
(59, '\r\nGestão Empresarial'),
(60, '\r\nHistória'),
(61, '\r\nHotelaria'),
(62, '\r\nInformática/T.I.'),
(63, '\r\nInternet'),
(64, '\r\nJornalismo'),
(65, '\r\nLetras'),
(66, '\r\nLogística'),
(67, '\r\nManutenção Industrial'),
(68, '\r\nManutenção Predial'),
(69, '\r\nMarketing'),
(70, '\r\nMatemática/Estatística'),
(71, '\r\nMedicina/Hospitalar'),
(72, '\r\nMeteorologia'),
(73, '\r\nMineração'),
(74, '\r\nModa'),
(75, '\r\nMuseologia'),
(76, '\r\nMúsica'),
(77, '\r\nNutrição'),
(78, '\r\nOceanografia'),
(79, '\r\nOdontologia'),
(80, '\r\nOrganização de Eventos/Produção Cultural'),
(81, '\r\nOrganização e Métodos'),
(82, '\r\nPesquisa de Mercado'),
(83, '\r\nPetrolífera'),
(84, '\r\nProdução/Fabricação'),
(85, '\r\nPropaganda'),
(86, '\r\nPsicologia'),
(87, '\r\nQualidade'),
(88, '\r\nQuímica'),
(89, '\r\nRadialismo e Televisão'),
(90, '\r\nRecursos Humanos'),
(91, '\r\nRelações Internacionais'),
(92, '\r\nRelações Públicas'),
(93, '\r\nSecretariado'),
(94, '\r\nSegurança e Saúde no Trabalho'),
(95, '\r\nSegurança Patrimonial'),
(96, '\r\nSeguros'),
(97, '\r\nServiço Social'),
(98, '\r\nServiços Administrativos'),
(99, '\r\nServiços Domésticos'),
(100, '\r\nServiços Especializados - Açougue'),
(101, '\r\nServiços Especializados - Padaria/Confeitaria'),
(102, '\r\nServiços Especializados - Peixaria'),
(103, '\r\nServiços Gerais'),
(104, '\r\nServiços Técnicos - Elétricos'),
(105, '\r\nServiços Técnicos - Eletrônicos'),
(106, '\r\nServiços Técnicos - Mecânicos'),
(107, '\r\nServiços Técnicos - Outros'),
(108, '\r\nSuprimentos'),
(109, '\r\nTelecomunicações'),
(110, '\r\nTerapia Ocupacional'),
(111, '\r\nTerceiro Setor/Responsabilidade Social'),
(112, '\r\nTradução/Interpretação'),
(113, '\r\nTransporte Aéreo'),
(114, '\r\nTransporte Marítimo'),
(115, '\r\nTransporte Terrestre'),
(116, '\r\nTributária'),
(117, '\r\nTurismo'),
(118, '\r\nVendas'),
(119, '\r\nVendas - Varejo'),
(120, '\r\nVendas Técnicas'),
(121, '\r\nVeterinária'),
(122, '\r\nWeb Design'),
(123, '\r\nZoologia'),
(124, '\r\nZootecnia'),
(125, 'Engenharia Elétrica'),
(126, 'Planejamento');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lista_areas`
--
ALTER TABLE `lista_areas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lista_areas`
--
ALTER TABLE `lista_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=127;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
