-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2016 at 10:50 AM
-- Server version: 5.5.38-35.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tiago275_talentos`
--

-- --------------------------------------------------------

--
-- Table structure for table `estados`
--

CREATE TABLE IF NOT EXISTS `estados` (
  `idestado` char(2) NOT NULL,
  `nome` varchar(25) DEFAULT NULL,
  `uf` char(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `estados`
--

INSERT INTO `estados` (`idestado`, `nome`, `uf`) VALUES
('01', 'Acre', 'AC'),
('02', 'Alagoas', 'AL'),
('03', 'Amazonas', 'AM'),
('04', 'Amapá', 'AP'),
('05', 'Bahia', 'BA'),
('06', 'Ceará', 'CE'),
('07', 'Distrito Federal', 'DF'),
('08', 'Espírito Santo', 'ES'),
('09', 'Goiás', 'GO'),
('10', 'Maranhão', 'MA'),
('11', 'Minas Gerais', 'MG'),
('12', 'Mato Grosso do Sul', 'MS'),
('13', 'Mato Grosso', 'MT'),
('14', 'Pará', 'PA'),
('15', 'Paraíba', 'PB'),
('16', 'Pernambuco', 'PE'),
('17', 'Piauí', 'PI'),
('18', 'Paraná', 'PR'),
('19', 'Rio de Janeiro', 'RJ'),
('20', 'Rio Grande do Norte', 'RN'),
('21', 'Rondônia', 'RO'),
('22', 'Roraima', 'RR'),
('23', 'Rio Grande do Sul', 'RS'),
('24', 'Santa Catarina', 'SC'),
('25', 'Sergipe', 'SE'),
('26', 'São Paulo', 'SP'),
('27', 'Tocantins', 'TO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`idestado`), ADD UNIQUE KEY `XPKestado` (`idestado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
