-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 19, 2016 at 10:49 AM
-- Server version: 5.5.38-35.2
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tiago275_talentos`
--

-- --------------------------------------------------------

--
-- Table structure for table `lista_niveis_profissionais`
--

CREATE TABLE IF NOT EXISTS `lista_niveis_profissionais` (
  `id` int(11) NOT NULL,
  `nivel` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lista_niveis_profissionais`
--

INSERT INTO `lista_niveis_profissionais` (`id`, `nivel`) VALUES
(1, 'Auxiliar'),
(2, '\r\nTécnico'),
(3, '\r\nEstágio'),
(4, 'Júnior'),
(5, 'Trainee'),
(6, 'Pleno'),
(7, 'Supervisão/Coordenação'),
(8, 'Gerência'),
(9, 'Diretoria'),
(10, 'Sênior'),
(11, 'Especialista');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lista_niveis_profissionais`
--
ALTER TABLE `lista_niveis_profissionais`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lista_niveis_profissionais`
--
ALTER TABLE `lista_niveis_profissionais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
